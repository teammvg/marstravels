
package marstravels.InformationClasses;

/**
 * Metoder för information om hotellen på Mars
 * @author pepsi
 */
public class HotelOnMarsWithFood {
    /**
     * Metod för information och pris om Hotell Polar Lansdorp
     * @return info 
     */
    public static String HotelPolarLansdorp(){
        String info = " Hotell Polar Lansdorp är ett enkelt hotell där man delar rum med andra människor (6-bäddsrum).\n"
                +"\n"
                +"Varje rum har egen dusch och toalett. Det finns en matsal där måltiderna serveras vid bestämda tider.\n"
                +"Hotellet har flera olika lounger där man kan umgås, njuta vid den konstgjorda brasan i öppna spisen, läsa böcker, lyssna på musik eller spela spel.\n"
                +"Det finns också en träningslokal för både kondition och styrketräning, samt gruppövningar.\n"
                +"Pris: Enkelbädd 3500 kr/bädd. ";
                return info;
                              
    }
    /**
     * Metod för infortmaion och pris om Hotell Polar Wielders
     * @return info
     */
    public static String HotelPolarWielders(){
        String info = "Hotell Polar Wielders är ett hotell där man delar rum med andra människor (4-bäddsrum).\n"
                +"\n"
                +"Varje rum har egen dusch och toalett. Det finns en matsal där måltiderna serveras vid bestämda tider.\n"
                +"\n"
                +"\nHotellet har flera olika lounger där man kan umgås, njuta vid den konstgjorda brasan i öppna spisen, läsa böcker, lyssna på musik eller spela spel.\n"
                +"\n"
                +"\nDet finns också en träningslokal för både kondition och styrketräning, samt gruppövningar.\n"
                +"Programmet för vistelsen är träning på morgonen efter frukost. Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                +"Pris: Dubbelrum 5000 kr/bädd ";
                return info;
    }
    /**
     * Metod för infortmaion och pris om Hotell Phobos
     * @return info
     */
    public static String HotelPhobos(){
        String info = " Hotell Phobos ligger vid ekvatorn på Mars och har både enkelrum och dubbelrum med egen dusch och toalett.\n"
                +"Varje rum är också inredd med en liten soffa och tv (filmer, serier och spel finns tillgängliga). \n"
                +"I restaurangen serveras måltiderna, men man kan också få maten serverad på rummen. \n"
                +"Flera olika lounger med olika teman finns så som bibliotek, biljardrum (där även andra spel finns), musikrum och bio.\n"
                +"Träningslokal med redskap för styrke- och konditionsträning finns samt tränare för gruppövningar.\n"
                +"Programmet för vistelsen är träning på morgonen efter frukost. Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                +"Pris: Enkelrum: 7500 kr Dubbelrum: 12 000 kr ";
        return info;
    }
    /**
     * Metod för infortmaion och pris om Hotell Deimos
     * @return info
     */
    public static String HotelDeimos(){
        String info = " Hotell Deimos ligger vid ekvatorn på Mars och har både enkelrum och dubbelrum med egen dusch och toalett.\n"
                +"Varje rum är också inredd med en liten soffa och tv (filmer, serier och spel finns tillgängliga). \n"
                +"I restaurangen serveras måltiderna, men man kan också få maten serverad på rummen.\n"
                +"Flera olika lounger med olika teman finns så som bibliotek, biljardrum (där även andra spel finns), musikrum och bio.\n"
                +"Träningslokal med redskap för styrke- och konditionsträning finns samt tränare för gruppövningar.\n"
                +"Programmet för vistelsen är träning på morgonen efter frukost. Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                +"Pris: Enkelrum: 7500 kr Dubbelrum: 12 000 kr.";
        return info;
    }
    /**
     * Metod för infortmaion och pris om Hotell Royal City
     * @return info
     */
    public static String StringRoyalCity(){
        String info = "Royal City är ett charmigt lyxhotell insprängt i berget i dalgången i Valles Marineris.\n"
                +"Här finns sviter med dubbelsäng och vardagsrum där man kan få samtliga måltider serverade.\n"
                +"Lyxrum med enkel eller dubbelbäddar och mini-loungedel finns också att tillgå.\n"
                +"Restaurangen har de bästa kockarna och maten som serveras är inte bara en njutning smakmässigt utan även hälsosam.\n"
                +"Lyxigt inredda bibliotek, teatersalong, lounge för att umgås och träningslokaler där flertalet personliga tränare är redo att skapa passande träningsprogram.\n"
                +"I närheten finns en galleria. Programmet för vistelsen är träning på morgonen efter frukost. Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                +"Pris: Enkel Lyx: 20 000 kr Dubbel Lyx: 35 000 kr Svit: 50 000 kr.";
        return info;
                

    }
}
