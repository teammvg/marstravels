

package marstravels.InformationClasses;


/**
 * Metod som håller information om planeten Mars.
 * @author pepsi
 */
public class AboutMars {
    /**
     * 
     * @return info
     */
    public static String mars(){
         String info = "Mars är den fjärde planeten från solen och kallas för den röda planeten. \n" + 
                 "Den röda färgen beror på de stora mängder järnoxid som finns fördelat över ytan och i atmosfären.  \n" +
                 " \n" + 
                 "Mars har många vulkaner (inga aktiva), dalgångar, vidsträckta slätter och iskalotter vid polerna.\n"+ 
                 "Det största berget i solsystemet hittar vi på Mars som dessutom är planetens största vulkan. \n" +
                 "Olympus Mons (Olympusberget) är ca 22-27 km högt, 550 km brett och ligger i en 2 km djup sänka.  \n" + 
                 " \n" +
                 "Mars har två månar, Phobos och Deimos, är små och har en oregelbunden form.  \n" +
                 " \n" +
                 "Ett år på Mars är 687 jorddygn och det gör att årstiderna är dubbelt så långa som mot jorden.  \n";
         
    
    return info;
    }
}
