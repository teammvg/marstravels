
package marstravels.InformationClasses;

/**
 * This class contains info about the accomodation/cabins onboard the spaceship
 *
 * @author Petter Gullin
 * @version 2018-03-02
 */
public class OnboardRoomsInformation {

    /**
     * This method returns the information about the suites onboard
     * @return The info is returned as a String
     */
    public static String suiteInfo() {
        String info = "Svit – hytt med utsikt över rymden, separat vardags- och sovrum inklusive\n"
                + "dubbelsäng. Dessutom skärm med stort utbud av film, serier, musik och spel.\n"
                + "Badrum med badkar/dusch och toalett. Frukost ingår i priset.\n"
                + "Pris: 1 200 000 kr/pers (enkelresa)";
        return info;

    }

    /**
     * This method returns the information about the spaceside cabins onboard
     * @return The info is returned as a String
     */
    public static String spacesideInfo() {
        String info = "Spaceside – hytt med utsikt över rymden, dubbelsäng. Dessutom skärm med stort\n"
                + "utbud av film, serier, musik och spel. Badrum med dusch och toalett.\n"
                + "Pris: 700 000 kr/pers (enkelresa)";
        return info;
    }

    /**
     * This method returns the information about the inside cabins onboard
     * @return The info is returned as a String
     */
    public static String insideInfo() {
        String info = "Inside – Insideshytt med två underbäddar som kan ändras till soffa dagtid.\n"
                + "Badrum med dusch och toalett.\n"
                + "Pris: 300 000 kr/pers (enkelresa)";
        return info;
    }

    /**
     * This method returns the information about the economy cabins onboard
     * @return The info is returned as a String
     */
    public static String economyInfo() {
        String info = "Economy – Insideshytt med 4 bäddar (två under- och överbäddar) där\n"
                + "underbäddarna kan ändras till soffa dagtid. Dusch finns till hytten, men toaletter\n"
                + "finns i korridoren.\n"
                + "Pris: 180 000 kr/pers (enkelresa)";
        return info;
    }

    /**
     * This method returns the information about the sleep capsules onboard
     * @return The info is returned as a String
     */
    public static String sleepCapsuleInfo() {
        String info = "Om man önskar kan man bli sövd under hela resans gång.\n"
                + "Den sövdes status och hälsa övervakas hela tiden av läkare.\n"
                + "Pris: 2 500 000 kr/pers (enkelresa)";
        return info;
    }
}
