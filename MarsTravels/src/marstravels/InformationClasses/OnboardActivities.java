package marstravels.InformationClasses;

/**
 *This class contains methods for all the activities onboard
 * @author Petter Gullin
 * @version 2018-03-02
 */
public class OnboardActivities {
    
   /**
    * This method returns info about all the free activities onboard
    * No paramters are needed
    * @return The info is returned as a string
    */
    public static String freeActivities(){
    String info = "Exempel på saker att göra ombord"
            +"\n"
            +"Ett välfyllt bibliotek, och ett spelrum med alla typer av spel."
            +"\n"
            +"En 25-metersbasäng som är gratis men tid måste bokas"
            +"\n"
            +"Det finns ett antal olika intresseföreningar som bokklubbar, sportföreningar, dramaklubbar och körer."
            +"\n"
            +"ombord arrangeras även olika kurser av intresseförenngarna inom deras olika områden."
            +"\n"
            +"Till deras förfogande finns en konsertlokal som är gratis att använda."
            +"\n"
            +"En biograf som visar gamla klassiska filmer (äldre än 2 år) "
            +"\n"
            +"Vid vår affärsgata hittar ni butiker av olika slag som tillhandahåller allt som man kan behöva.\n"
            +"Man handlar med vår speciella betalkort som man köper och laddar innan avresan.\n"
            +"I butikerna finns varor som kläder, smink, juveler, böcker och tidningar, godis och snacks, leksaker, teknikprylar.\n"
            + "Det finns också frisörer, skräddare, skomakare och spa för den som vill utnyttja det.\n"
            + "Ombord finns även ett gym för att hålla sig i form under resan, det är gratis att utnyttja.";
    return info;
}
    /**
     * Returns the info about the moviepremiers onboard
     * @return The info is returned as a String
     */
   public static String movieInfo(){
    String info = "Filmpremiärer av nya filmer visas en gång i månaden. Val av 1-6 biljetter.";
    return info; 
   } 
   /**
    * This method returns the information about the concerts onboard
    * @return The info is returned as a String
    */
   public static String concertInfo(){
    String info = "En Konsert med kända artister ges varannan månad. Val av 1-3 biljetter.";
    return info; 
   }
   /**
    * This method returns the information about the theater-premiers onboard
    * @return The info is returned as a String
    */
   public static String theaterInfo(){
    String info = "En teaterpremiär visas varannan månad. Val av 1-3 biljetter.";
    return info; 
}
}