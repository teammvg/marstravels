
package marstravels.InformationClasses;

import javax.swing.JOptionPane;

/**
 * Metod som visar vad för typ av mat respektive restaurang har
 * @author pepsi
 */
public class FoodPackagesMeny {

    Object[] possibilities = {"MarsDonald", "Marsian Buffé",
        "Tellus Home", "Spaceview"};

    public void getMeny() {
        String restaurants = (String) JOptionPane.showInputDialog(
                null,
                "\"Se meny\"",
                "Välj ett av alternativen",
                JOptionPane.PLAIN_MESSAGE,
                null,
                possibilities,
                "MarsDonald");

        if (restaurants == "MarsDonald") {
            System.out.println("MarsDonald har 2 olika frukostar, 3 olika luncher och 4 olika middagar, /n"
                    + "samt 3 olika kakor/desserter att tillgå mellan måltiderna. ");
        } else if (restaurants == "Marsian Buffé") {
            System.out.println("Marsian Buffé är restaurangen som har buffé dygnet runt med olika rätter som kan passa oavsett tid på dagen./n"
                    + "Alltifrån våfflor och omeletter till grillat och pizza.");
        } else if (restaurants == "Tellus Home") {
            System.out.println("Tellus Home serverar en enklare frukostbuffé, 5 olika luncher (kött/fisk/veg/soppa/pasta) och 6 olika middagar (2 alternativ av fisk/kött/veg). /n"
                    + "Deras desserbuffé som är öppen mellan måltiderna består av 3-5 olika kakor/desserter plus dryck. ");
        } else if (restaurants == "Spaceview") {
            System.out.println("Spaceview restaurangen har specialiserat sig på ny och kreativ mat där kockarna tar favoriträtter från jorden /n"
                    + "och med inspiration från rymden gör om dem och ger dig en ny smakupplevelse. ");
        }
  
    }
   
}
    


