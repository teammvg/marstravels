package marstravels;

import java.io.Serializable;

/**
 *
 * @author Elias
 */
public class UserAccount implements Serializable {

    private static String username;
    private static String password;

    public UserAccount(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserAccount() {
    }
    

    public static String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    
}
