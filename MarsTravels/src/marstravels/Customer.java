package marstravels;

import java.io.Serializable;

/**
 *
 * @author Elias
 */
public class Customer implements Serializable {

    private String customerId;
    private String firstName;
    private String lastName;
    private String adress;
    private String identityNumber;
    private String email;
    private String phoneNumber;
    private String healthDescription;

    public Customer() {

    }

    public Customer(String customerId, String firstName, String lastName, String adress, String identityNumber, String email, String phoneNumber, String healthDescription) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.adress = adress;
        this.identityNumber = identityNumber;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.healthDescription = healthDescription;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFullName() {
        return this.firstName + this.lastName;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHealthDescription() {
        return healthDescription;
    }

    public void setHealthDescription(String healthDescription) {
        this.healthDescription = healthDescription;
    }

    @Override
    public String toString() {
        return "Kunduppgifter" + "\nKundnummer: " + this.getCustomerId() + "\n"
                + "Namn: " + this.getFullName() + "\n"
                + "Adress: " + this.getAdress() + "\n"
                + "Personnummer: " + this.getIdentityNumber() + "\n"
                + "Mail: " + this.getEmail() + "\n"
                + "Telefonnummer: " + this.getPhoneNumber();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
