package marstravels;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

/**
 *
 * @author Elias
 */
public class CustomerOrder implements Serializable {

    private String customerId;
    private Calendar departureDate;
    private Calendar returnDate;
    private String ferryRoom;
    private String foodPackage;
    private String movieTicket;
    private String theatreTicket;
    private String consertTicket;
    private String hotelRoom;
    private int debitCard;
    private boolean healthInsurance;
    private int totalCost;

    public CustomerOrder(String customerId) {
        this.customerId = customerId;
    }

    public CustomerOrder(String customerId, Calendar departureDate, String ferryRoom, String foodPackage, String movieTicket, String theatreTicket, String consertTicket, String hotelRoom, int debitCard, boolean healthInsurance) {
        this.customerId = customerId;
        this.departureDate = departureDate;
        this.ferryRoom = ferryRoom;
        this.foodPackage = foodPackage;
        this.movieTicket = movieTicket;
        this.theatreTicket = theatreTicket;
        this.consertTicket = consertTicket;
        this.hotelRoom = hotelRoom;
        this.debitCard = debitCard;
        this.healthInsurance = healthInsurance;
    }

    public int getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(int debitCard) {
        if (debitCard < 20000) {
            //Systemet protesterar.
        } else {
            this.debitCard = debitCard;
        }
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Calendar getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Calendar departureDate) {
        this.departureDate = departureDate;
    }

    public String getFerryRoom() {
        return ferryRoom;
    }

    public void setFerryRoom(String ferryRoom) {
        this.ferryRoom = ferryRoom;
    }

    public String getFoodPackage() {
        return foodPackage;
    }

    public void setFoodPackage(String foodPackage) {
        this.foodPackage = foodPackage;
    }

    public String getMovieTicket() {
        return movieTicket;
    }

    public void setMovieTicket(String movieTicket) {
        this.movieTicket = movieTicket;
    }

    public String getTheatreTicket() {
        return theatreTicket;
    }

    public void setTheatreTicket(String theatreTicket) {
        this.theatreTicket = theatreTicket;
    }

    public String getConsertTicket() {
        return consertTicket;
    }

    public void setConsertTicket(String consertTicket) {
        this.consertTicket = consertTicket;
    }

    public String getHotelRoom() {
        return hotelRoom;
    }

    public void setHotelRoom(String hotelRoom) {
        this.hotelRoom = hotelRoom;
    }

    public boolean isHealthInsurance() {
        return healthInsurance;
    }

    public void setHealthInsurance(boolean healthInsurance) {
        this.healthInsurance = healthInsurance;
    }

    public Calendar getArrivalDate() {
        Calendar arrivalDate = Calendar.getInstance();
        arrivalDate.set(getDepartureDate().get(1), getDepartureDate().get(2) + 6, getDepartureDate().get(5), 12, 0, 0);
        return arrivalDate;
    }

    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar returnDate) {
        this.returnDate = returnDate;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public String toString() {
        return "Orderuppgifter" + "\nKundnummer: " + customerId + "\n"
                + "Hälsoförsäkring: " + isHealthInsurance() + "\n"
                + "Avresedatum: " + this.getDepartureDate().getTime() + "\n"
                + "Hytt: " + this.getFerryRoom() + "\n"
                + "Matpaket: " + this.getFoodPackage() + "\n"
                + "Filmbiljetter: " + this.getMovieTicket() + "\n"
                + "Teaterbiljetter: " + this.getTheatreTicket() + "\n"
                + "Konsertbiljetter: " + this.getTheatreTicket() + "\n"
                + "Hotell: " + this.getHotelRoom() + "\n"
                + "Betalkort: " + this.getDebitCard() + "\n"
                + "Antal dagar borta: " + ChronoUnit.DAYS.between(this.getDepartureDate().toInstant(), this.getReturnDate().toInstant()) + "\n"
                + "Totalsumma: " + this.getTotalCost();
    }

}
