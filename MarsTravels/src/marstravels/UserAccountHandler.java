package marstravels;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Elias
 */
public class UserAccountHandler implements Serializable {
    
    private static String userLogin;
    static HashMap<String, UserAccount> accountRegister = new HashMap();

    public static void createAccount(String username, String password) {
       accountRegister.put(username, new UserAccount(username, password));
    }

    public static boolean Login(String username, String password) {
        if (correctUserAccount(username, password) == true) {
            setUser(username);
            return true;
        }
        return false;
    }

    public static boolean correctUsername(String username) {
        return accountRegister.containsKey(username);
    }

    public static boolean correctPassword(String username, String password) {
        return accountRegister.get(username).getPassword().equals(password);
    }

    public static boolean correctUserAccount(String username, String password) {
        return correctUsername(username) && correctPassword(username, password);
    }

    public static void setUser(String username) {
        UserAccountHandler.userLogin = username;
    }

    public static String getCurrentUser() {
        return UserAccountHandler.userLogin;
    }
}
