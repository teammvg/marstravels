/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import marstravels.Database;
import marstravels.OrderProcessor;

/**
 *
 * @author emile
 */
public class HotellOnMars_GUI {

    public static void hotellOnMars() throws IOException, FileNotFoundException, ClassNotFoundException {
        // skapar ramen frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblHeaderPage6 = new JLabel("Boende på Mars");
        lblHeaderPage6.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeaderPage6.setBounds(300, 0, 800, 150);
        //lblHeaderPage6.setLocation(400, 0);
        frame.add(lblHeaderPage6);

        final Choice bio1 = new Choice();
        bio1.setBounds(10, 50, 180, 75);
        bio1.setLocation(50, 66);

        OrderProcessor o = new Database().get();

//här fyller man i vad lista ska bestå av 
        frame.add(bio1);

        //samma som ovan fast rullistan heter lyx
        final Choice bio2 = new Choice();
        bio2.setBounds(10, 50, 180, 75);
        bio2.setLocation(50, 66);

        bio2.setVisible(false);
        frame.add(bio2);

        o.getAvailableHotelRoomList(o.getSelectedDepartureDate(), o.getSelectedReturnDate()).entrySet().forEach((entry) -> {
            bio1.add(entry.toString());
            bio2.add(entry.toString());
        });

        //ruta som visar person 1
        JLabel lblPerson1 = new JLabel("Resenär 1");
        lblPerson1.setSize(70, 10);
        lblPerson1.setLocation(50, 50);
        frame.add(lblPerson1);

        //ruta som visar person 2
        JLabel lblPerson21 = new JLabel("Resenär 2");
        lblPerson21.setSize(70, 10);
        lblPerson21.setLocation(50, 50);
        lblPerson21.setVisible(false);
        frame.add(lblPerson21);

        //knappen som tar dig vidare till nästa sida
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        HemResa_GUI.hemResa();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(HotellOnMars_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        JTextArea infoRutaHPW = new JTextArea("Hotell Polar Wielders är ett hotell där man delar rum med andra människor (4-bäddsrum).\n"
                + "\n"
                + "Varje rum har egen dusch och toalett. Det finns en matsal där måltiderna serveras vid bestämda tider.\n"
                + "\n"
                + "\nHotellet har flera olika lounger där man kan umgås, njuta vid den konstgjorda brasan i öppna spisen,"
                + "\n" + "läsa böcker, lyssna på musik eller spela spel.\n"
                + "\n"
                + "\nDet finns också en träningslokal för både kondition och styrketräning, samt gruppövningar.\n"
                + "Programmet för vistelsen är träning på morgonen efter frukost."
                + "\n" + "Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                + "Pris: Dubbelrum 5000 kr/bädd ");
        infoRutaHPW.setBounds(300, 100, 570, 250);
        infoRutaHPW.setVisible(false);
        infoRutaHPW.setEditable(false);
        frame.add(infoRutaHPW);

        JTextArea infoRutaHPL = new JTextArea(" Hotell Polar Lansdorp är ett enkelt hotell där man delar rum med andra människor (6-bäddsrum).\n"
                + "\n"
                + "Varje rum har egen dusch och toalett. Det finns en matsal där måltiderna serveras vid bestämda tider.\n"
                + "Hotellet har flera olika lounger där man kan umgås, njuta vid den konstgjorda brasan i öppna spisen,"
                + "\n" + "läsa böcker, lyssna på musik eller spela spel.\n"
                + "Det finns också en träningslokal för både kondition och styrketräning, samt gruppövningar.\n"
                + "Pris: Enkelbädd 3500 kr/bädd. ");
        infoRutaHPL.setBounds(300, 100, 570, 250);
        infoRutaHPL.setVisible(false);
        infoRutaHPL.setEditable(false);
        frame.add(infoRutaHPL);

        JTextArea infoRutaHP = new JTextArea(" Hotell Phobos ligger vid ekvatorn på Mars och har både"
                + "\n" + "enkelrum och dubbelrum med egen dusch och toalett.\n"
                + "Varje rum är också inredd med en liten soffa och tv (filmer, serier och spel finns tillgängliga). \n"
                + "I restaurangen serveras måltiderna, men man kan också få maten serverad på rummen. \n"
                + "Flera olika lounger med olika teman finns så som bibliotek, biljardrum (där även andra spel finns),"
                + "\n" + "musikrum och bio.\n"
                + "Träningslokal med redskap för styrke- och konditionsträning finns samt tränare för gruppövningar.\n"
                + "Programmet för vistelsen är träning på morgonen efter frukost."
                + "\n" + "Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                + "Pris: Enkelrum: 7500 kr Dubbelrum: 12 000 kr ");
        infoRutaHP.setBounds(300, 100, 570, 250);
        infoRutaHP.setVisible(false);
        infoRutaHP.setEditable(false);
        frame.add(infoRutaHP);

        JTextArea infoRutaHD = new JTextArea("Hotell Deimos ligger vid ekvatorn på Mars och har både"
                + "\n" + "enkelrum och dubbelrum med egen dusch och toalett.\n"
                + "Varje rum är också inredd med en liten soffa och tv (filmer, serier och spel finns tillgängliga). \n"
                + "I restaurangen serveras måltiderna, men man kan också få maten serverad på rummen.\n"
                + "Flera olika lounger med olika teman finns så som bibliotek, biljardrum (där även andra spel finns),"
                + "\n" + "musikrum och bio.\n"
                + "Träningslokal med redskap för styrke- och konditionsträning finns samt tränare för gruppövningar.\n"
                + "Programmet för vistelsen är träning på morgonen efter frukost. "
                + "\n" + "Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                + "Pris: Enkelrum: 7500 kr Dubbelrum: 12 000 kr.");
        infoRutaHD.setBounds(300, 100, 570, 250);
        infoRutaHD.setVisible(false);
        infoRutaHD.setEditable(false);
        frame.add(infoRutaHD);

        JTextArea infoRutaSRC = new JTextArea("Royal City är ett charmigt lyxhotell insprängt i berget i dalgången i Valles Marineris.\n"
                + "Här finns sviter med dubbelsäng och vardagsrum där man kan få samtliga måltider serverade.\n"
                + "Lyxrum med enkel eller dubbelbäddar och mini-loungedel finns också att tillgå.\n"
                + "Restaurangen har de bästa kockarna och maten som serveras är inte bara en njutning smakmässigt"
                + "\n" + "utan även hälsosam.\n"
                + "Lyxigt inredda bibliotek, teatersalong, lounge för att umgås och träningslokaler där "
                + "\n" + "flertalet personliga tränare är redo att skapa passande träningsprogram.\n"
                + "I närheten finns en galleria. "
                + "\n" + "Programmet för vistelsen är träning på morgonen efter frukost. "
                + "\n" + "Detta då gravitation och syre på Mars gör att man måste hålla sig i form.\n"
                + "Pris: Enkel Lyx: 20 000 kr Dubbel Lyx: 35 000 kr Svit: 50 000 kr.");
        infoRutaSRC.setBounds(300, 100, 570, 250);
        infoRutaSRC.setVisible(false);
        infoRutaSRC.setEditable(false);
        frame.add(infoRutaSRC);

        //öppnar o stänger inforutan
        JToggleButton HotelPolarWielders = new JToggleButton("HotelPolarWielders");
        HotelPolarWielders.setBounds(50, 100, 150, 50);
        frame.add(HotelPolarWielders);
        ItemListener itemListener = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaHPW.setVisible(true); // show your message here
                    infoRutaHPL.setVisible(false);
                    infoRutaHP.setVisible(false);
                    infoRutaHD.setVisible(false);
                    infoRutaSRC.setVisible(false);

                } else {
                    infoRutaHPW.setVisible(false); // remove your message
                }
            }
        };
        HotelPolarWielders.addItemListener(itemListener);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHPL = new JToggleButton("HotelPolarLandsdorp");
        toggleButtonHPL.setBounds(50, 150, 150, 50);
        frame.add(toggleButtonHPL);
        ItemListener itemListenerHPL = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaHPL.setVisible(true); // show your message here
                    infoRutaHPW.setVisible(false);
                    infoRutaHP.setVisible(false);
                    infoRutaHD.setVisible(false);
                    infoRutaSRC.setVisible(false);
                } else {
                    infoRutaHPL.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonHPL.addItemListener(itemListenerHPL);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHP = new JToggleButton("HotelPhobos");
        toggleButtonHP.setBounds(50, 200, 150, 50);
        frame.add(toggleButtonHP);
        ItemListener itemListenerHP = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaHP.setVisible(true); // show your message here
                    infoRutaHPW.setVisible(false);
                    infoRutaHPL.setVisible(false);
                    infoRutaHD.setVisible(false);
                    infoRutaSRC.setVisible(false);

                } else {
                    infoRutaHP.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonHP.addItemListener(itemListenerHP);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHD = new JToggleButton("HotelDeimos");
        toggleButtonHD.setBounds(50, 250, 150, 50);
        frame.add(toggleButtonHD);
        ItemListener itemListenerHD = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaHD.setVisible(true); // show your message here
                    infoRutaHPW.setVisible(false);
                    infoRutaHPL.setVisible(false);
                    infoRutaHP.setVisible(false);
                    infoRutaSRC.setVisible(false);
                } else {
                    infoRutaHD.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonHD.addItemListener(itemListenerHD);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonSRC = new JToggleButton("Royal City");
        toggleButtonSRC.setBounds(50, 300, 150, 50);
        frame.add(toggleButtonSRC);
        ItemListener itemListenerSRC = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaSRC.setVisible(true); // show your message here
                    infoRutaHPW.setVisible(false);
                    infoRutaHPL.setVisible(false);
                    infoRutaHP.setVisible(false);
                    infoRutaHD.setVisible(false);

                } else {
                    infoRutaSRC.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonSRC.addItemListener(itemListenerSRC);

        //en bakåtknapp 8gå till förgående steg)
        JButton back = new JButton("bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        evenemang_GUI.evenemang();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(HotellOnMars_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //ruatn som visar totalsumman
        JTextField txtSumField = new JTextField("");
        txtSumField.setBounds(100, 100, 100, 50);
        txtSumField.setLocation(450, 400);
        txtSumField.setEditable(false);
        frame.add(txtSumField);

        //ruatn som visar totalsumman
        JLabel lblTotal = new JLabel("Totalsumma:");
        lblTotal.setBounds(100, 100, 100, 50);
        lblTotal.setLocation(350, 400);
        frame.add(lblTotal);

        //den gör inget... tar man bort den omplaceras den ovan....
        JLabel txtFieldTotalSumma = new JLabel();
        txtFieldTotalSumma.setBounds(100, 100, 100, 50);
        txtFieldTotalSumma.setLocation(350, 400);
        frame.add(txtFieldTotalSumma);

        JButton buttonPerson2 = new JButton("P2");
        buttonPerson2.setBounds(100, 8, 50, 30);
        frame.add(buttonPerson2);
        buttonPerson2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson21.setVisible(true);
                    bio2.setVisible(true);
                    lblPerson1.setVisible(false);
                    bio1.setVisible(false);

                } else {
                    lblPerson21.setVisible(false);
                    bio2.setVisible(false);

                }
            }
        });

        JButton buttonPerson1 = new JButton("P1");
        buttonPerson1.setBounds(50, 8, 50, 30);
        frame.add(buttonPerson1);
        buttonPerson1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson1.setVisible(true);
                    bio1.setVisible(true);
                    lblPerson21.setVisible(false);
                    bio2.setVisible(false);

                } else {
                    lblPerson1.setVisible(false);
                    bio1.setVisible(false);

                }
            }
        });
        JButton buttonPerson = new JButton();
        buttonPerson.setBounds(50, 15, 50, 30);
        buttonPerson.setVisible(false);
        frame.add(buttonPerson);

        txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");

        bio1.addItemListener((ItemEvent e) -> {

            o.getCurrentCustomerOrder("1").setHotelRoom(e.getItem().toString().split("=")[0]);
            o.getCurrentCustomerOrder("1").setTotalCost(o.getTotalCost(o.getCurrentCustomerOrder("1")));
            txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
            System.out.println(o.getArticle().get(e.getItem().toString().split("=")[0]).getPrice());
            try {
                new Database().set(o);
            } catch (IOException ex) {
                Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        //properties för ramen
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
