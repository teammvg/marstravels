/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import marstravels.Database;
import marstravels.OrderProcessor;

/**
 *
 * @author emile
 */
public class Hyttalternativ_GUI {

    public static void hyttAlternativ() throws IOException, ClassNotFoundException, FileNotFoundException {

// skapar ramen frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblHeaderPage3 = new JLabel("Hyttalternativ");
        lblHeaderPage3.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeaderPage3.setBounds(380, 0, 800, 150);
        //lblHeaderPage2.setLocation(400, 0);
        frame.add(lblHeaderPage3);

//skapar en ruta med text som heter label
        final Label label = new Label();
        label.setSize(40, 100);

        //gör en rulllista som heter hotell1 (set bounds bestämmer storlek och location betsämmer plats)
        final Choice hotell = new Choice();
        hotell.setBounds(40, 200, 125, 75);
        hotell.setLocation(50, 90);

//här fyller man i vad lista ska bestå av
        frame.add(hotell);

        JLabel lblPerson1 = new JLabel("Resenär 1");

        lblPerson1.setSize(70, 10);
        lblPerson1.setLocation(50, 60);
        frame.add(lblPerson1);

//samma som ovan fast rullistan heter hotell2
        final Choice hotell2 = new Choice();
        hotell2.setBounds(40, 200, 125, 75);
        hotell2.setLocation(50, 90);

        hotell2.setVisible(false);
        frame.add(hotell2);

        OrderProcessor o = new Database().get();

        System.out.println(o.getCurrentCustomer().size());

        o.getAvailableFerryRoomList(o.getSelectedDepartureDate().getTime().toString(), "departure").entrySet().forEach((entry) -> {

            hotell.add(entry.toString());
            hotell2.add(entry.toString());
        });

        /*  
        //en textruta som säger "person 2"
        JLabel lblPerson2 = new JLabel("Person 2");
        lblPerson2.setSize(70, 10);
        lblPerson2.setLocation(50, 170);
        frame.add(lblPerson2);

         */
        JLabel lblPerson2 = new JLabel("Resenär 2");
        lblPerson2.setSize(70, 10);
        lblPerson2.setLocation(50, 60);
        lblPerson2.setVisible(false);
        frame.add(lblPerson2);

        JTextArea infoRutaSovkapsel = new JTextArea("Om man önskar kan man bli sövd under hela resans gång.\n"
                + "Den sövdes status och hälsa övervakas hela tiden av läkare.\n"
                + "Pris: 2 500 000 kr/pers (enkelresa)");
        infoRutaSovkapsel.setBounds(350, 100, 570, 250);
        infoRutaSovkapsel.setVisible(false);
        infoRutaSovkapsel.setEditable(false);
        frame.add(infoRutaSovkapsel);

        JTextArea infoRutaSvit = new JTextArea("Svit – hytt med utsikt över rymden, separat vardags- och sovrum inklusive\n"
                + "dubbelsäng. Dessutom skärm med stort utbud av film, serier, musik och spel.\n"
                + "Badrum med badkar/dusch och toalett. Frukost ingår i priset.\n"
                + "Pris: 1 200 000 kr/pers (enkelresa)");
        infoRutaSvit.setBounds(350, 100, 570, 250);
        infoRutaSvit.setVisible(false);
        infoRutaSvit.setEditable(false);
        frame.add(infoRutaSvit);

        JTextArea infoRutaEkonomi = new JTextArea("Economy – Insideshytt med 4 bäddar (två under- och överbäddar) där\n"
                + "underbäddarna kan ändras till soffa dagtid. Dusch finns till hytten, men toaletter\n"
                + "finns i korridoren.\n"
                + "Pris: 180 000 kr/pers (enkelresa)");
        infoRutaEkonomi.setBounds(350, 100, 570, 250);
        infoRutaEkonomi.setVisible(false);
        infoRutaEkonomi.setEditable(false);
        frame.add(infoRutaEkonomi);

        JTextArea infoRutaSpace = new JTextArea("Spaceside – hytt med utsikt över rymden, dubbelsäng. Dessutom skärm med stort\n"
                + "utbud av film, serier, musik och spel. Badrum med dusch och toalett.\n"
                + "Pris: 700 000 kr/pers (enkelresa)");
        infoRutaSpace.setBounds(350, 100, 570, 250);
        infoRutaSpace.setVisible(false);
        infoRutaSpace.setEditable(false);
        frame.add(infoRutaSpace);

        JTextArea infoRutaS = new JTextArea("Inside – Insideshytt med två underbäddar som kan ändras till soffa dagtid.\n"
                + "Badrum med dusch och toalett.\n"
                + "Pris: 300 000 kr/pers (enkelresa)");
        infoRutaS.setBounds(350, 100, 570, 250);
        infoRutaS.setVisible(false);
        infoRutaS.setEditable(false);
        frame.add(infoRutaS);

        //öppnar o stänger inforutan
        JToggleButton HotelPolarWielders = new JToggleButton("Info om sovkapsel");
        HotelPolarWielders.setBounds(200, 100, 150, 50);
        frame.add(HotelPolarWielders);
        ItemListener itemListener = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaSovkapsel.setVisible(true);
                    infoRutaEkonomi.setVisible(false);
                    infoRutaS.setVisible(false);
                    infoRutaSvit.setVisible(false);
                    infoRutaSpace.setVisible(false);

                } else {
                    infoRutaSovkapsel.setVisible(false); // remove your message
                }
            }
        };
        HotelPolarWielders.addItemListener(itemListener);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHPL = new JToggleButton("Info om svit");
        toggleButtonHPL.setBounds(200, 150, 150, 50);
        frame.add(toggleButtonHPL);
        ItemListener itemListenerHPL = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaSvit.setVisible(true);
                    infoRutaEkonomi.setVisible(false);
                    infoRutaS.setVisible(false);
                    infoRutaSovkapsel.setVisible(false);
                    infoRutaSpace.setVisible(false);

                } else {
                    infoRutaSvit.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonHPL.addItemListener(itemListenerHPL);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHP = new JToggleButton("Info om ekonomi");
        toggleButtonHP.setBounds(200, 200, 150, 50);
        frame.add(toggleButtonHP);
        ItemListener itemListenerHP = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaEkonomi.setVisible(true);
                    infoRutaSpace.setVisible(false);
                    infoRutaS.setVisible(false);
                    infoRutaSovkapsel.setVisible(false);
                    infoRutaSvit.setVisible(false);

                } else {
                    infoRutaEkonomi.setVisible(false); // remove your message
                }
            }
        };
        toggleButtonHP.addItemListener(itemListenerHP);

        //öppnar o stänger inforutan
        JToggleButton toggleButtoni = new JToggleButton("Info om Spaceside");
        toggleButtoni.setBounds(200, 250, 150, 50);
        frame.add(toggleButtoni);
        ItemListener itemListeneri = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaSpace.setVisible(true);
                    infoRutaS.setVisible(false);
                    infoRutaEkonomi.setVisible(false); // show your message here
                    infoRutaSovkapsel.setVisible(false);
                    infoRutaSvit.setVisible(false);

                } else {
                    infoRutaSpace.setVisible(false); // remove your message
                }
            }
        };
        toggleButtoni.addItemListener(itemListeneri);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonin = new JToggleButton("Info om inside ");
        toggleButtonin.setBounds(200, 300, 150, 50);
        frame.add(toggleButtonin);
        ItemListener itemListenerin = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaS.setVisible(true);
                    infoRutaSpace.setVisible(false);
                    infoRutaEkonomi.setVisible(false); // show your message here
                    infoRutaSovkapsel.setVisible(false);
                    infoRutaSvit.setVisible(false);

                } else {

                    infoRutaS.setVisible(false);
                }
            }
        };
        toggleButtonin.addItemListener(itemListenerin);

        //knappen som tar dig vidare till nästa sida
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        Matpaket_GUI.matPaket();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //en bakåtknapp (gå till förgående steg)
        JButton back = new JButton("bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        BookingPage1.pageGUI();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //rutan som ska visa totalkostnad
        JTextField txtSumField = new JTextField();
        txtSumField.setBounds(100, 100, 100, 50);
        txtSumField.setLocation(500, 400);
        txtSumField.setEditable(false);
        frame.add(txtSumField);

        //ruta som visar att totalsumman visas bredvid
        JLabel lblTotalSumma = new JLabel("TotalSumma:");
        lblTotalSumma.setBounds(300, 500, 150, 150);
        lblTotalSumma.setLocation(400, 350);
        frame.add(lblTotalSumma);

        JButton buttonPerson2 = new JButton("P2");
        buttonPerson2.setBounds(100, 15, 50, 30);
        frame.add(buttonPerson2);
        buttonPerson2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {

                    lblPerson2.setVisible(true);
                    hotell2.setVisible(true);
                    lblPerson1.setVisible(false);
                    hotell.setVisible(false);

                } else {
                    lblPerson2.setVisible(false);
                    hotell2.setVisible(false);

                }

                lblPerson2.setVisible(true);
                hotell2.setVisible(true);
                lblPerson1.setVisible(false);
                hotell.setVisible(false);

            }

        });

        JButton buttonPerson1 = new JButton("P1");
        buttonPerson1.setBounds(50, 15, 50, 30);
        frame.add(buttonPerson1);
        buttonPerson1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {

                    lblPerson1.setVisible(true);
                    hotell.setVisible(true);
                    lblPerson2.setVisible(false);
                    hotell2.setVisible(false);

                } else {
                    lblPerson1.setVisible(false);
                    hotell.setVisible(false);

                }

                lblPerson1.setVisible(true);
                hotell.setVisible(true);
                lblPerson2.setVisible(false);
                hotell2.setVisible(false);

            }
        });
        JButton buttonPerson = new JButton();
        buttonPerson.setBounds(50, 15, 50, 30);
        buttonPerson.setVisible(false);
        frame.add(buttonPerson);

        txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
        //System.out.println(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");

        hotell.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                System.out.println(ChronoUnit.DAYS.between(o.getSelectedDepartureDate().toInstant(), o.getSelectedReturnDate().toInstant()) / 30);
                o.getCurrentCustomerOrder("1").setFerryRoom(e.getItem().toString().split("=")[0]);
                try {
                    new Database().set(o);
                } catch (IOException ex) {
                    Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                //System.out.print(o.getCurrentCustomerOrder("1").toString());
                txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");

            }
        });

        //lägger till listorna på ramen och bestämmer ramens properties
        //  frame.add(label); 
        //f.add(b);  
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
