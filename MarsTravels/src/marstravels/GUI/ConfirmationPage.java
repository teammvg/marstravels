package marstravels.GUI;


import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import marstravels.Database;
import marstravels.OrderProcessor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Sidan till MarsTravels för sammanställning utav alla tidigare val som kräver
 * bekräftning och slutför bokningen.
 *
 * @author nilsi
 */
public class ConfirmationPage extends JFrame {


    public static void pageGUI() throws IOException, ClassNotFoundException, FileNotFoundException {

        OrderProcessor o = new Database().get();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel();

        // Knapp för att slutföra bokningen, bokningen måste
        //bekräftas innan knappen är klickbar
        JButton fram = new JButton("Boka");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        fram.setEnabled(false);
        frame.add(fram);
        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);

                    MainProgramPage.pageGUI();
                }

            }
        });

        // Knapp för att gå bakåt i programmet
        JButton back = new JButton("bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
        frame.add(back);
        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        HemResa_GUI.hemResa();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(ConfirmationPage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        // Rubriken för bokningsbekräftelsesidan
        JLabel lblHeader = new JLabel("Bokningsbekräftelse");
        lblHeader.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeader.setBounds(350, -10, 300, 100);
        frame.add(lblHeader);

        // Label för person nr 1
        JLabel person1 = new JLabel("Bokning för person 1");
        person1.setBounds(175, 40, 150, 50);
        person1.setVisible(true);
        frame.add(person1);

        // Textfält som skall sammanställa informationen för person nr 1s bokning
        JTextArea bekräftelseInfoPerson1 = new JTextArea(o.getCurrentCustomer("1").toString()+"\n"+o.getCurrentCustomerOrder("1").toString());
        bekräftelseInfoPerson1.setBounds(100, 80, 350, 360);
        bekräftelseInfoPerson1.setVisible(true);
        bekräftelseInfoPerson1.setEditable(false);
        frame.add(bekräftelseInfoPerson1);

        //Label för person nr 2
        JLabel person2 = new JLabel("Bokning för person 2");
        person2.setBounds(575, 40, 150, 50);
        person2.setVisible(true);
        frame.add(person2);
        // Textfält som skall sammanställa informationen för person nr 2s bokning
        JTextArea bekräftelseInfoPerson2 = new JTextArea("test\ntest\ntest\ntest\ntest\ntest\ntest\ntest\ntest\ntest\ntest\n");
        bekräftelseInfoPerson2.setBounds(500, 80, 350, 360);
        bekräftelseInfoPerson2.setVisible(true);
        bekräftelseInfoPerson2.setEditable(false);
        frame.add(bekräftelseInfoPerson2);

        //textrutan som visar totala summan
        JTextField txtField = new JTextField(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
        txtField.setBounds(450, 450, 100, 50);
        txtField.setEditable(false);
        frame.add(txtField);

        //textrutan som visar att den totala summan står bredvid
        JLabel lblTotalSumma = new JLabel("TotalSumma:");
        lblTotalSumma.setBounds(350, 450, 100, 50);
        lblTotalSumma.setVisible(true);
        frame.add(lblTotalSumma);

        // Checkruta som ber att användaren skall bekräfta ordern
        JCheckBox confirmOrder = new JCheckBox("Bekräfta ordern?");
        confirmOrder.setBounds(400, 510, 130, 30);
        confirmOrder.setVisible(true);
        frame.add(confirmOrder);

        // ItemListener som känner av ifall checkrutan är ikryssad eller ej,
        // knappen för att slutföra bokningen kräver att rutan är ikryssad.
        ItemListener itemListenerHP = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    System.out.print("Bokningen är bekräftad\n");
                    fram.setEnabled(true);
                } else {
                    fram.setEnabled(false);

                }

            }

        };
        confirmOrder.addItemListener(itemListenerHP);

        frame.add(mainPanel);
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
