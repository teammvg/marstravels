/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author emile
 */
public class SökaBokning {
    
    public static void searchBokning(){
    
    // skapar ramen frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
        
         JLabel lblPerson2 = new JLabel("Sök personnummer:");
        lblPerson2.setBounds(60, 100, 200, 50);
        frame.add(lblPerson2);
        
        JTextField search = new JTextField();  
        search.setBounds(200, 110, 200, 30);
        frame.add(search);
        
        JLabel lblavresa = new JLabel("Sök datum för avresa:");
        lblavresa.setBounds(60, 150, 200, 50);
        frame.add(lblavresa);
        
        JTextField searchAvresa = new JTextField();  
        searchAvresa.setBounds(200, 160, 200, 30);
        frame.add(searchAvresa);
        
        JLabel lblshow = new JLabel("Här visas den sökta bokningen:");
        lblshow.setBounds(400, 70, 200, 50);
        frame.add(lblshow);
        
         
        
          JTextField show = new JTextField();  
        show.setBounds(400, 110, 300, 100);
        frame.add(show);
        
         //en bakåt knapp för att gå till förgående steg
        JButton back = new JButton("Bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        frame.add(back);
        back.setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    MainProgramPage.pageGUI();

                }

            }
        });
        
        JTextField search2 = new JTextField();  
        search2.setBounds(190, 150, 200, 30);
        search2.setVisible(false);
        frame.add(search2);
    
        
        
     frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
    
    
    
    
}
