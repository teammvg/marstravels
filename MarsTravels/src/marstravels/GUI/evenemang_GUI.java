/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import marstravels.Database;
import marstravels.OrderProcessor;

/**
 *
 * @author emile
 */
public class evenemang_GUI {

    public static void evenemang() throws IOException, ClassNotFoundException, IOException {

        OrderProcessor o = new Database().get();

        // skapar ramen frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblHeaderPage5 = new JLabel("Val av antal evenmangsbiljetter");
        lblHeaderPage5.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeaderPage5.setBounds(250, 0, 800, 150);
        //lblHeaderPage5.setLocation(400, 0);
        frame.add(lblHeaderPage5);

        JTextArea infoRutaBetalkort = new JTextArea("Man laddar kortet med hur mycket pengar man vill men minsta beloppet är 20 000 kr."
                + "\nMed detta kort handlar du i affärerna och hos läkaren om ingen försäkring valts.");
        infoRutaBetalkort.setBounds(350, 150, 500, 100);
        // infoRutaBetalkort.setVisible(false);
        infoRutaBetalkort.setEditable(false);
        frame.add(infoRutaBetalkort);

        JTextArea infoRutaBio = new JTextArea("Filmpremiärer av nya filmer visas en gång i månaden. Val av 1-6 biljetter.");
        infoRutaBio.setBounds(350, 100, 570, 250);
        infoRutaBio.setVisible(false);
        infoRutaBio.setEditable(false);
        frame.add(infoRutaBio);

        JTextArea infoRutaKonsert = new JTextArea("En Konsert med kända artister ges varannan månad. Val av 1-3 biljetter.");
        infoRutaKonsert.setBounds(350, 100, 570, 250);
        infoRutaKonsert.setVisible(false);
        infoRutaKonsert.setEditable(false);
        frame.add(infoRutaKonsert);

        JTextArea infoRutaTeater = new JTextArea("En teaterpremiär visas varannan månad. Val av 1-3 biljetter.");
        infoRutaTeater.setBounds(350, 100, 570, 250);
        infoRutaTeater.setVisible(false);
        infoRutaTeater.setEditable(false);
        frame.add(infoRutaTeater);

        JTextArea infoRutaFriaAktiviteter = new JTextArea("Exempel på saker att göra ombord"
                + "\n"
                + "Ett välfyllt bibliotek, och ett spelrum med alla typer av spel."
                + "\n"
                + "En 25-metersbasäng som är gratis men tid måste bokas"
                + "\n"
                + "Det finns ett antal olika intresseföreningar som bokklubbar, sportföreningar, dramaklubbar och körer."
                + "\n"
                + "ombord arrangeras även olika kurser av intresseförenngarna inom deras olika områden."
                + "\n"
                + "Till deras förfogande finns en konsertlokal som är gratis att använda."
                + "\n"
                + "En biograf som visar gamla klassiska filmer (äldre än 2 år) "
                + "\n"
                + "Vid vår affärsgata hittar ni butiker av olika slag som tillhandahåller allt som man kan behöva.\n"
                + "Man handlar med vår speciella betalkort som man köper och laddar innan avresan.\n"
                + "I butikerna finns varor som kläder, smink, juveler, "
                + "\n" + "böcker och tidningar, godis och snacks, leksaker, teknikprylar.\n"
                + "Det finns också frisörer, skräddare, skomakare och spa för den som vill utnyttja det.\n"
                + "Ombord finns även ett gym för att hålla sig i form under resan, det är gratis att utnyttja.");
        infoRutaFriaAktiviteter.setBounds(350, 100, 570, 250);
        infoRutaFriaAktiviteter.setVisible(false);
        infoRutaFriaAktiviteter.setEditable(false);
        frame.add(infoRutaFriaAktiviteter);

        //öppnar o stänger inforutan
        JToggleButton Bio = new JToggleButton("Info om bio");
        Bio.setBounds(150, 100, 170, 50);
        frame.add(Bio);
        ItemListener itemListener = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaBio.setVisible(true); // show your message here
                    infoRutaKonsert.setVisible(false);
                    infoRutaTeater.setVisible(false);
                    infoRutaFriaAktiviteter.setVisible(false);
                    infoRutaBetalkort.setVisible(false);

                } else {
                    infoRutaBio.setVisible(false); // remove your message
                    infoRutaBetalkort.setVisible(true);
                }
            }
        };

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHPL = new JToggleButton("Info om konsert");
        toggleButtonHPL.setBounds(150, 150, 170, 50);
        frame.add(toggleButtonHPL);
        ItemListener itemListenerHPL = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaKonsert.setVisible(true); // show your message here
                    infoRutaBio.setVisible(false);
                    infoRutaTeater.setVisible(false);
                    infoRutaFriaAktiviteter.setVisible(false);
                    infoRutaBetalkort.setVisible(false);

                } else {
                    infoRutaKonsert.setVisible(false); // remove your message
                    infoRutaBetalkort.setVisible(true);
                }
            }
        };

        toggleButtonHPL.addItemListener(itemListenerHPL);
        Bio.addItemListener(itemListener);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHP = new JToggleButton("Info om teater");
        toggleButtonHP.setBounds(150, 200, 170, 50);
        frame.add(toggleButtonHP);
        ItemListener itemListenerHP = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaTeater.setVisible(true); // show your message here
                    infoRutaBio.setVisible(false);
                    infoRutaKonsert.setVisible(false);
                    infoRutaFriaAktiviteter.setVisible(false);
                    infoRutaBetalkort.setVisible(false);

                } else {
                    infoRutaTeater.setVisible(false); // remove your message
                    infoRutaBetalkort.setVisible(true);
                }
            }
        };
        toggleButtonHP.addItemListener(itemListenerHP);

        //öppnar o stänger inforutan
        JToggleButton toggleButtonHD = new JToggleButton("Info om fria aktiviteter");
        toggleButtonHD.setBounds(150, 250, 170, 50);
        frame.add(toggleButtonHD);
        ItemListener itemListenerHD = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRutaFriaAktiviteter.setVisible(true); // show your message here
                    infoRutaBio.setVisible(false);
                    infoRutaKonsert.setVisible(false);
                    infoRutaTeater.setVisible(false);
                    infoRutaBetalkort.setVisible(false);

                } else {
                    infoRutaFriaAktiviteter.setVisible(false); // remove your message
                    infoRutaBetalkort.setVisible(true);
                }
            }
        };
        toggleButtonHD.addItemListener(itemListenerHD);

        //gör en rulllista som heter budget (set bounds bestämmer storlek och location)
        final Choice bio1 = new Choice();
        bio1.setBounds(10, 50, 100, 75);
        bio1.setLocation(50, 90);
//här fyller man i vad lista ska bestå av
        frame.add(bio1);
        /**
         *
         * //samma som ovan fast rullistan heter lyx final Choice konsert1=new
         * Choice(); konsert1.setBounds(10,200, 75,705);
         * konsert1.setLocation(50, 115); konsert1.add("Lyx 1");
         * konsert1.add("Hotell2"); konsert1.add("Hotell3");
         * konsert1.add("Hotell4"); konsert1.add("Hotell5");
         * frame.add(konsert1);
         */
//samma som ovan fast rullistan heter lyx
        final Choice bio2 = new Choice();
        bio2.setBounds(10, 50, 125, 75);
        bio2.setLocation(50, 90);

        bio2.setVisible(false);
        frame.add(bio2);

        o.getArticle().values().forEach((article) -> {
            if (article.getCategory().equals("Evenemangsbiljett")) {
                bio1.add(article.getName());
                bio2.add(article.getName());
            }
        });

        /**
         *
         *
         *
         * final Choice konsert2=new Choice(); konsert2.setBounds(10,200,
         * 75,705); konsert2.setLocation(50, 115); konsert2.add("2 biljetter");
         * konsert2.add("Hotell2"); konsert2.add("Hotell3");
         * konsert2.add("Hotell4"); konsert2.add("Hotell5");
         * konsert2.setVisible(false); frame.add(konsert2);
         */
        //ruta som visar person 1
        JLabel lblPerson1 = new JLabel("Resenär 1");
        lblPerson1.setSize(70, 10);
        lblPerson1.setLocation(50, 60);
        frame.add(lblPerson1);

        //ruta som visar person 2
        JLabel lblPerson21 = new JLabel("Resenär 2");
        lblPerson21.setSize(70, 10);
        lblPerson21.setLocation(50, 60);
        lblPerson21.setVisible(false);
        frame.add(lblPerson21);

        JLabel lblPerson221 = new JLabel();
        lblPerson221.setSize(70, 10);
        lblPerson221.setLocation(50, 170);
        lblPerson221.setVisible(false);
        frame.add(lblPerson221);

        //gör inget ......... tar man bort den omplaceras den textrutan som står ovan (bugg)
        JLabel lblPerson2 = new JLabel();
        lblPerson2.setSize(70, 10);
        lblPerson2.setLocation(50, 100);
        frame.add(lblPerson2);

        //knappen som tar dig vidare till nästa sida
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    try {
                        frame.setVisible(false);
                        HotellOnMars_GUI.hotellOnMars();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(evenemang_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //en bakåtknapp 8gå till förgående steg)
        JButton back = new JButton("bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        Matpaket_GUI.matPaket();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(evenemang_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //ruatn som visar totalsumman
        JTextField txtSumField = new JTextField();
        txtSumField.setBounds(100, 100, 100, 50);
        txtSumField.setLocation(450, 400);
        txtSumField.setEditable(false);
        frame.add(txtSumField);

        //rutan som visar att totalsumman ska visas bredvid
        JLabel txtFieldTotalSumma = new JLabel("Totalsumma:");
        txtFieldTotalSumma.setBounds(100, 100, 100, 50);
        txtFieldTotalSumma.setLocation(350, 400);
        frame.add(txtFieldTotalSumma);

        //classic ..  gör inget men rutan ovan omplaceras om man tar bort  (bugg)
        JLabel lblTotala = new JLabel();
        lblTotala.setBounds(300, 500, 150, 150);
        lblTotala.setLocation(350, 350);
        frame.add(lblTotala);

        JLabel belopp = new JLabel("Fyll i önskat belopp [enter] ");
        belopp.setBounds(370, 70, 200, 100);
        frame.add(belopp);

        JTextField betalkortP1 = new JTextField();
        betalkortP1.setBounds(570, 110, 200, 20);
        frame.add(betalkortP1);

        JTextField betalkortP2 = new JTextField();
        betalkortP2.setBounds(570, 110, 200, 20);
        betalkortP2.setVisible(false);
        frame.add(betalkortP2);

        JButton buttonPerson2 = new JButton("P2");
        buttonPerson2.setBounds(100, 15, 50, 30);
        frame.add(buttonPerson2);
        buttonPerson2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson21.setVisible(true);
                    //konsert2.setVisible(true);
                    bio2.setVisible(true);
                    betalkortP2.setVisible(true);
                    lblPerson1.setVisible(false);
                    // konsert1.setVisible(false);
                    bio1.setVisible(false);

                } else {
                    lblPerson21.setVisible(false);
                    bio2.setVisible(false);
                    //konsert2.setVisible(false);
                    betalkortP2.setVisible(false);

                }
            }
        });

        JButton buttonPerson1 = new JButton("P1");
        buttonPerson1.setBounds(50, 15, 50, 30);
        frame.add(buttonPerson1);
        buttonPerson1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson1.setVisible(true);
                    // konsert1.setVisible(true);
                    bio1.setVisible(true);
                    betalkortP1.setVisible(true);

                    lblPerson21.setVisible(false);
                    bio2.setVisible(false);
                    //konsert2.setVisible(false);
                    betalkortP2.setVisible(false);

                } else {
                    lblPerson1.setVisible(false);
                    //konsert1.setVisible(false);
                    bio1.setVisible(false);
                    betalkortP1.setVisible(false);

                }
            }
        });
        JButton buttonPerson = new JButton();
        buttonPerson.setBounds(50, 15, 50, 30);
        buttonPerson.setVisible(false);
        frame.add(buttonPerson);

        betalkortP1.addActionListener((ActionEvent e) -> {
            int bel = Integer.parseInt(e.getActionCommand());
            if (bel < 20000) {
                belopp.setText("Fel! Belopp mindre än 20000.");

            } else {
                belopp.setText("Önskad kredit: " + bel);

                o.getCurrentCustomerOrder("1").setDebitCard(bel);
                txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
                try {
                    new Database().set(o);
                } catch (IOException ex) {
                    Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            System.out.println(e.getActionCommand());
        });

        bio1.addItemListener((ItemEvent e) -> {
            System.out.println(e);

            o.getCurrentCustomerOrder("1").setTheatreTicket(bio1.getSelectedItem());
            System.out.println("k");

            System.out.println(e.getItem());

            txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
            try {
                new Database().set(o);
            } catch (IOException ex) {
                Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        
        
        txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");

        //properties för ramen
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
