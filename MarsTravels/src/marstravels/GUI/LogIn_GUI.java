/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import marstravels.UserAccountHandler;
import marstravels.GUI.MainProgramPage.*;

/**
 *
 * @author emile
 */
public class LogIn_GUI {
    

 
     
public static void password(){
    
 JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Mars Travels - Booking system");
        
       JPasswordField password = new JPasswordField("password");   
       JLabel lblPassword=new JLabel("Lösenord:");    
        lblPassword.setBounds(350,250, 100,30);    
        password.setBounds(450,250,100,30);    
        frame.add(lblPassword);
        frame.add(password);
        password.selectAll();
        
        JLabel lblHeaderLogin = new JLabel("Mars Travels");
        lblHeaderLogin.setFont(new Font("Serif", Font.PLAIN, 50));
        lblHeaderLogin.setBounds(350, 0, 800, 150);
        //lblHeaderLogin.setLocation(400, 0);
        frame.add(lblHeaderLogin);
        
        JLabel lblerror = new JLabel ("Felaktigt användarnamn eller lösenord!");
        lblerror.setFont(new Font("Serif", Font.PLAIN, 16));
        lblerror.setBounds(360, 270, 800, 150);
        frame.add(lblerror);
        lblerror.setVisible(false);
        lblerror.setForeground(Color.red);
        
JLabel lblHeaderLogin2 = new JLabel("Ange inloggningsuppgifter:");
        lblHeaderLogin2.setFont(new Font("Serif", Font.PLAIN, 20));
        lblHeaderLogin2.setBounds(370, 70, 800, 150);
        //lblHeaderLogin2.setLocation(400, 0);
        frame.add(lblHeaderLogin2);
        
        JLabel lblUserName = new JLabel("Användarnamn:");
        lblUserName.setBounds(350,200, 100,30);
        JTextField userName = new JTextField("");
        userName.setBounds(450, 200, 100, 30);
        
        JButton logInButton = new JButton("Log in");
        logInButton.setBounds(450, 300, 70, 30);
        frame.add(logInButton);
        logInButton.addActionListener(new ActionListener() {
           
            String correctUser = "User123";
            char[] correctPass = {'P','a','s','s','1','2','3'};
            
            
            public void actionPerformed(ActionEvent e) {
                if (logInButton.isEnabled()) {
                    System.out.println(userName.getText());
                    String user = userName.getText();
                    char[] passIn = password.getPassword();
                    //String pass =  passIn.toString();
                    System.out.println();
                    if(correctUser.equals(user)&&(Arrays.equals(passIn, correctPass))) {
                        frame.setVisible(false);
                        MainProgramPage.pageGUI();
                        //Hyttalternativ_GUI.hyttAlternativ(); //Fel sida!! Enbart som test efteresom startsidan ej är klar 
                    }else{lblerror.setVisible(true);
                        
                    }
                    
                }

            }
        });
        
        
        frame.add(lblUserName);
        frame.add(userName);
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        
        
        
                
     
        
           


        }
    
}
