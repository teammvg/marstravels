/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import marstravels.Customer;
import marstravels.CustomerOrder;
import marstravels.Database;
import marstravels.OrderProcessor;

/**
 * Sidan som visas för användaren när man klickar på knappen "Bokning" på
 * "MainProgramPage"
 *
 * @author nilsi
 */
public class BookingPage1 extends JFrame {

    public static void pageGUI() throws IOException, ClassNotFoundException {

        OrderProcessor o = new Database().get();
//skapar ramen för fönstret, "EXIT_ON_CLOSE" stänger programet när man kryssar uppe i höger hörn
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //alla textfält, huvudpanelen för fönstret och checkboxen för hälsoförsäkring
        JTextField txtUserName, txtEmail, txtEmailP2, txtTelefon, txtTelefonP2, txtBirthdate, txtBirthdateP2, txtAdressP2, txtAdress;
        JPanel mainPanel = new JPanel();
        JTextField txtTraveler;
        JCheckBox chkBoxSaveConnection, chkBoxChronicalDisorder, chkBoxChronicalDisorderP2, chkBoxHealthInsurance, chkBoxHealthInsuranceP2, chkBoxMedicine, chkBoxMedicineP2;

        //textrutan som säger 
        JLabel lbldatumet = new JLabel("Välj hemresedatum");
        lbldatumet.setBounds(60, 50, 120, 20);
        frame.add(lbldatumet);

        JLabel ankomstTillMars = new JLabel("ankomst på mars: ");
        ankomstTillMars.setBounds(60, 420, 130, 30);
        frame.add(ankomstTillMars);

        JTextField datumet = new JTextField();
        datumet.setBounds(190, 420, 200, 30);
        datumet.setEditable(false);
        frame.add(datumet);

        final Choice hemresa = new Choice();
        hemresa.setBounds(180, 50, 204, 50);
        frame.add(hemresa);

        
        new Database().get().getReturnDate().values().forEach((date) -> {
            hemresa.add(date.getTime().toString());
        });
//här fyller man i vad lista ska bestå av

        JLabel lblHeaderPage2 = new JLabel("Ange Personuppgifter");
        lblHeaderPage2.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeaderPage2.setBounds(350, 80, 500, 50);
        //lblHeaderPage2.setLocation(400, 0);
        frame.add(lblHeaderPage2);

        JTextArea infoRutaHealth = new JTextArea("Vid sjukdom under resan eller på Mars så råder gällande taxa hos"
                + "läkaren/tandläkaren."
                + "\n" + " Kunden kan köpa våran försäkring som tar hand om all eventuell sjukvård, "
                + "\n" + "hälsoförsäkringen kostar 50 000 kr per person."
                + "\n" + "Om kunden har kroniska problem (fråga kunden) som tvingar"
                + "\n" + "denna att inta medicin varje dag måste en hälsoförsäkran"
                + "\n" + "fyllas i och skickas in för bedömning av"
                + "försäkringens totala belopp."
        );
        infoRutaHealth.setBounds(450, 150, 500, 250);
        infoRutaHealth.setVisible(true);
        infoRutaHealth.setEditable(false);
        frame.add(infoRutaHealth);

        //textrutan som visar totala summan
        JTextField txtSumField = new JTextField("");
        txtSumField.setBounds(450, 450, 100, 50);
        txtSumField.setEditable(false);
        frame.add(txtSumField);

        //textrutan som visar att den totala summan står bredvid
        JLabel lblTotalSumma = new JLabel("TotalSumma:");
        lblTotalSumma.setBounds(350, 450, 100, 50);
        frame.add(lblTotalSumma);

        //textruta som visar att det gäller person 1
        JLabel lblPerson1 = new JLabel("Uppgifter om resenär 1");
        lblPerson1.setBounds(60, 100, 200, 50);
        frame.add(lblPerson1);

        JLabel lblPerson2 = new JLabel("Uppgifter om resenär 2");
        lblPerson2.setBounds(60, 100, 200, 50);
        frame.add(lblPerson2);

        // täxtfälten där man skriver in för och efternamn
        //rutan som förklarar att för och efternamn ska fyllas i
        JTextField txtfirstName = new JTextField(15);
        JTextField txtLastName = new JTextField(15);
        JLabel enterName = new JLabel("Fyll i förnamn");
        JLabel enterLastName = new JLabel("Fyll i efternamn");
        enterName.setBounds(60, 150, 130, 30);
        enterLastName.setBounds(60, 180, 130, 30);
        txtLastName.setBounds(190, 180, 200, 30);
        txtfirstName.setBounds(190, 150, 200, 30);
        frame.add(enterName);
        frame.add(enterLastName);
        frame.add(txtfirstName);
        frame.add(txtLastName);

        txtAdress = new JTextField(15);
        txtAdress.setBounds(190, 240, 200, 30);
        //rutan som förklarar att man ska fylla i resenärens adress
        JLabel enterAdress = new JLabel("Fyll i adress");
        enterAdress.setBounds(60, 240, 130, 30);
        frame.add(enterAdress);
        frame.add(txtAdress);

        //textfältet där man skriver in resenärens mail
        txtEmail = new JTextField(15);
        txtEmail.setBounds(190, 300, 200, 30);
        frame.add(txtEmail);

        //rutan som förklarar att resenärens mail ska fyllas i
        JLabel enterEmail = new JLabel("Fyll i E-postadress");
        enterEmail.setBounds(60, 300, 130, 30);
        frame.add(enterEmail);

        //textfältet där man skriver in telefonnummer och
        //rutan som förklarar att man ska fylla i resenärens telefonnummer
        txtTelefon = new JTextField(15);
        JLabel phoneNumber = new JLabel("Fyll i telefonnummer");
        phoneNumber.setBounds(60, 270, 130, 30);
        frame.add(phoneNumber);
        txtTelefon.setBounds(190, 270, 200, 30);
        frame.add(txtTelefon);

        //textfältet där man fyller i resenärens personnummer
        txtBirthdate = new JTextField(15);
        txtBirthdate.setBounds(190, 210, 200, 30);
        frame.add(txtBirthdate);

        //rutan som förklarar att man ska fylla i resenärens personnummer
        JLabel enterBirthdate = new JLabel("Fyll i personnummer");
        enterBirthdate.setBounds(60, 210, 130, 30);
        frame.add(enterBirthdate);

        //checkboxen om kronisk sjukdom
        chkBoxChronicalDisorder = new JCheckBox("Kronisk sjukdom?");
        chkBoxChronicalDisorder.setBounds(60, 390, 130, 30);
        frame.add(chkBoxChronicalDisorder);
        // chkChronicalDisorder.addActionListener(this);

        chkBoxMedicine = new JCheckBox("Mediciner?");
        chkBoxMedicine.setBounds(60, 360, 130, 30);
        frame.add(chkBoxMedicine);
        // chkBoxMedicine.addActionListener(this);

        //checkboxen där man kryssar i om man vill ha hälsoförsäkring
        chkBoxHealthInsurance = new JCheckBox("hälsoförsäkring?");
        chkBoxHealthInsurance.setBounds(60, 330, 130, 30);
        frame.add(chkBoxHealthInsurance);
        // chkBoxMedicine.addActionListener(this);

        JTextField txtMedicin = new JTextField();
        txtMedicin.setBounds(190, 360, 200, 30);
        frame.add(txtMedicin);

        JTextField txtKronisk = new JTextField();
        txtKronisk.setBounds(190, 390, 200, 30);
        frame.add(txtKronisk);

        //rutan man fyller i antal resenärer
        //  txtTraveler = new JTextField("", 15);
        //txtTraveler.setBounds(180, 30, 70, 20);
        //frame.add(txtTraveler);
        /////////ANTAL RESENÄRER
        final Choice antalPersoner = new Choice();
        antalPersoner.setBounds(180, 10, 204, 50);
        frame.add(antalPersoner);
//här fyller man i vad lista ska bestå av

        antalPersoner.add("1 resenär");
        antalPersoner.add("2 resenärer");

        //textrutan som säger "fyll i antal resenärer"
        JLabel lblAntal = new JLabel("Välj antal resenärer");
        lblAntal.setBounds(60, 10, 170, 20);
        frame.add(lblAntal);

        //rullistan för att välja datum
        final Choice listChoice = new Choice();
        listChoice.setBounds(180, 30, 204, 50);
        frame.add(listChoice);

//här fyller man i vad lista ska bestå av
        new Database().get().getDepartureDate().values().forEach((date) -> {
            listChoice.add(date.getTime().toString());
        });

        //textruta som visar "välj avresedatum"
        JLabel lblAvresa = new JLabel("Välj avresedatum:");
        lblAvresa.setBounds(60, 30, 170, 20);
        frame.add(lblAvresa);

        // täxtfälten där man skriver in för och efternamn
        //rutan som förklarar att för och efternamn ska fyllas i
        JTextField txtfirstNameP2 = new JTextField(15);
        JTextField txtLastNameP2 = new JTextField(15);
        JLabel enterNameP2 = new JLabel("Fyll i förnamn");
        JLabel enterLastNameP2 = new JLabel("Fyll i efternamn");
        enterNameP2.setBounds(60, 150, 130, 30);
        enterLastNameP2.setBounds(60, 180, 130, 30);
        txtLastNameP2.setBounds(190, 180, 200, 30);
        txtfirstNameP2.setBounds(190, 150, 200, 30);
        enterNameP2.setVisible(false);
        enterLastNameP2.setVisible(false);
        txtfirstNameP2.setVisible(false);
        txtLastNameP2.setVisible(false);
        frame.add(enterNameP2);
        frame.add(enterLastNameP2);
        frame.add(txtfirstNameP2);
        frame.add(txtLastNameP2);

        txtAdressP2 = new JTextField(15);
        txtAdressP2.setBounds(190, 240, 200, 30);
        //rutan som förklarar att man ska fylla i resenärens adress
        JLabel enterAdressP2 = new JLabel("Fyll i adress");
        enterAdressP2.setBounds(60, 240, 130, 30);
        enterAdressP2.setVisible(false);
        txtAdressP2.setVisible(false);
        frame.add(enterAdressP2);
        frame.add(txtAdressP2);

        //textfältet där man skriver in resenärens mail
        txtEmailP2 = new JTextField(15);
        txtEmailP2.setBounds(190, 300, 200, 30);
        txtEmailP2.setVisible(false);
        frame.add(txtEmailP2);

        //rutan som förklarar att resenärens mail ska fyllas i
        JLabel enterEmailP2 = new JLabel("Fyll i E-postadress");
        enterEmailP2.setBounds(60, 300, 130, 30);
        enterEmailP2.setVisible(false);
        frame.add(enterEmailP2);

        //textfältet där man skriver in telefonnummer och
        //rutan som förklarar att man ska fylla i resenärens telefonnummer
        txtTelefonP2 = new JTextField(15);
        JLabel phoneNumberP2 = new JLabel("Fyll i telefonnummer");
        phoneNumberP2.setBounds(60, 270, 130, 30);
        phoneNumberP2.setVisible(false);
        frame.add(phoneNumberP2);
        txtTelefonP2.setBounds(190, 270, 200, 30);
        txtTelefonP2.setVisible(false);
        frame.add(txtTelefonP2);

        //textfältet där man fyller i resenärens personnummer
        txtBirthdateP2 = new JTextField(15);
        txtBirthdateP2.setBounds(190, 210, 200, 30);
        txtBirthdateP2.setVisible(false);
        frame.add(txtBirthdateP2);

        //rutan som förklarar att man ska fylla i resenärens personnummer
        JLabel enterBirthdateP2 = new JLabel("Fyll i personnummer");
        enterBirthdateP2.setBounds(60, 210, 130, 30);
        enterBirthdateP2.setVisible(false);
        frame.add(enterBirthdateP2);

        //checkboxen om kronisk sjukdom
        chkBoxChronicalDisorderP2 = new JCheckBox("Kronisk sjukdom?");
        chkBoxChronicalDisorderP2.setBounds(60, 390, 130, 30);
        chkBoxChronicalDisorderP2.setVisible(false);
        frame.add(chkBoxChronicalDisorderP2);
        // chkChronicalDisorder.addActionListener(this);

        chkBoxMedicineP2 = new JCheckBox("Mediciner?");
        chkBoxMedicineP2.setBounds(60, 360, 130, 30);
        chkBoxMedicineP2.setVisible(false);
        frame.add(chkBoxMedicineP2);
        // chkBoxMedicine.addActionListener(this);

        //checkboxen där man kryssar i om man vill ha hälsoförsäkring
        chkBoxHealthInsuranceP2 = new JCheckBox("hälsoförsäkring?");
        chkBoxHealthInsuranceP2.setBounds(60, 330, 130, 30);
        chkBoxHealthInsuranceP2.setVisible(false);
        frame.add(chkBoxHealthInsuranceP2);
        // chkBoxMedicine.addActionListener(this);

        JTextField txtMedicinP2 = new JTextField();
        txtMedicinP2.setBounds(190, 360, 200, 30);
        txtMedicinP2.setVisible(false);
        frame.add(txtMedicinP2);

        JTextField txtKroniskP2 = new JTextField();
        txtKroniskP2.setBounds(190, 390, 200, 30);
        txtKroniskP2.setVisible(false);
        frame.add(txtKroniskP2);

        lblPerson2.setVisible(false);

        JButton buttonPerson2 = new JButton("P2");
        buttonPerson2.setBounds(100, 85, 50, 30);
        frame.add(buttonPerson2);
        buttonPerson2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    txtEmailP2.setVisible(true);
                    txtAdressP2.setVisible(true);
                    enterEmailP2.setVisible(true);
                    txtfirstNameP2.setVisible(true);
                    txtLastNameP2.setVisible(true);
                    enterNameP2.setVisible(true);
                    txtTelefonP2.setVisible(true);
                    phoneNumberP2.setVisible(true);
                    enterBirthdateP2.setVisible(true);
                    txtMedicinP2.setVisible(true);
                    txtKroniskP2.setVisible(true);
                    txtBirthdateP2.setVisible(true);
                    chkBoxChronicalDisorderP2.setVisible(true);
                    chkBoxHealthInsuranceP2.setVisible(true);
                    chkBoxMedicineP2.setVisible(true);
                    lblPerson2.setVisible(true);

                    txtEmail.setVisible(false);
                    txtAdress.setVisible(false);
                    enterEmail.setVisible(false);
                    txtfirstName.setVisible(false);
                    txtLastName.setVisible(false);
                    enterName.setVisible(false);
                    txtTelefon.setVisible(false);
                    phoneNumber.setVisible(false);
                    enterBirthdate.setVisible(false);
                    txtMedicin.setVisible(false);
                    txtKronisk.setVisible(false);
                    txtBirthdate.setVisible(false);
                    chkBoxChronicalDisorder.setVisible(false);
                    chkBoxHealthInsurance.setVisible(false);
                    chkBoxMedicine.setVisible(false);
                    lblPerson1.setVisible(false);
                } else {
                    txtEmailP2.setVisible(false);
                    txtAdressP2.setVisible(false);
                    enterEmailP2.setVisible(false);
                    txtfirstNameP2.setVisible(false);
                    txtLastNameP2.setVisible(false);
                    enterNameP2.setVisible(false);
                    txtTelefonP2.setVisible(false);
                    phoneNumberP2.setVisible(false);
                    enterBirthdateP2.setVisible(false);
                    txtMedicinP2.setVisible(false);
                    txtKroniskP2.setVisible(false);
                    txtBirthdateP2.setVisible(false);
                    chkBoxChronicalDisorderP2.setVisible(false);
                    chkBoxHealthInsuranceP2.setVisible(false);
                    chkBoxMedicineP2.setVisible(false);
                    lblPerson2.setVisible(false);

                }

            }
        });
        JButton buttonPerson1 = new JButton("P1");
        buttonPerson1.setBounds(50, 85, 50, 30);
        frame.add(buttonPerson1);
        buttonPerson1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    txtEmail.setVisible(true);
                    txtAdress.setVisible(true);
                    enterEmail.setVisible(true);
                    txtfirstName.setVisible(true);
                    txtLastName.setVisible(true);
                    enterName.setVisible(true);
                    txtTelefon.setVisible(true);
                    phoneNumber.setVisible(true);
                    enterBirthdate.setVisible(true);
                    txtMedicin.setVisible(true);
                    txtKronisk.setVisible(true);
                    txtBirthdate.setVisible(true);
                    chkBoxChronicalDisorder.setVisible(true);
                    chkBoxHealthInsurance.setVisible(true);
                    chkBoxMedicine.setVisible(true);
                    lblPerson1.setVisible(true);

                    txtEmailP2.setVisible(false);
                    txtAdressP2.setVisible(false);
                    enterEmailP2.setVisible(false);
                    txtfirstNameP2.setVisible(false);
                    txtLastNameP2.setVisible(false);
                    enterNameP2.setVisible(false);
                    txtTelefonP2.setVisible(false);
                    phoneNumberP2.setVisible(false);
                    enterBirthdateP2.setVisible(false);
                    txtMedicinP2.setVisible(false);
                    txtKroniskP2.setVisible(false);
                    txtBirthdateP2.setVisible(false);
                    chkBoxChronicalDisorderP2.setVisible(false);
                    chkBoxHealthInsuranceP2.setVisible(false);
                    chkBoxMedicineP2.setVisible(false);
                    lblPerson2.setVisible(false);
                } else {
                    txtEmail.setVisible(false);
                    txtAdress.setVisible(false);
                    enterEmail.setVisible(false);
                    txtfirstName.setVisible(false);
                    txtLastName.setVisible(false);
                    enterName.setVisible(false);
                    txtTelefon.setVisible(false);
                    phoneNumber.setVisible(false);
                    enterBirthdate.setVisible(false);
                    txtMedicin.setVisible(false);
                    txtKronisk.setVisible(false);
                    txtBirthdate.setVisible(false);
                    chkBoxChronicalDisorder.setVisible(false);
                    chkBoxHealthInsurance.setVisible(false);
                    chkBoxMedicine.setVisible(false);
                    lblPerson1.setVisible(false);

                }
            }
        });

        //.........Knappar.........
        //knappen som gör så att programmet kan fortsätta til nästa steg
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);
                    try {

                        o.setSelectedDepartureDate(listChoice.getSelectedItem());
                        o.setSelectedReturnDate(hemresa.getSelectedItem());

                        //Uppgifter för kund 1
                        o.setCurrentCustomer("1", new Customer(txtBirthdate.getText(), txtfirstName.getText(),
                                txtLastName.getText(), txtAdress.getText(), txtBirthdate.getText(),
                                txtEmail.getText(), txtTelefon.getText(), txtKronisk.getText()));
                        o.setCurrentCustomerOrder("1", new CustomerOrder(o.getCurrentCustomer("1").getIdentityNumber()));
                        o.getCurrentCustomerOrder("1").setHealthInsurance(chkBoxHealthInsurance.isSelected());
                        o.getCurrentCustomerOrder("1").setDepartureDate(o.getSelectedDepartureDate());
                        o.getCurrentCustomerOrder("1").setReturnDate(o.getSelectedReturnDate());
                        //Uppgifter för kund 2
                             o.setCurrentCustomer("2", new Customer(txtBirthdateP2.getText(), txtfirstNameP2.getText(),
                                txtLastNameP2.getText(), txtAdressP2.getText(), txtBirthdateP2.getText(),
                                txtEmailP2.getText(), txtTelefonP2.getText(), txtKroniskP2.getText()));
                        o.setCurrentCustomerOrder("2", new CustomerOrder(o.getCurrentCustomer("2").getIdentityNumber()));
                        o.getCurrentCustomerOrder("2").setHealthInsurance(chkBoxHealthInsuranceP2.isSelected());
                        o.getCurrentCustomerOrder("2").setDepartureDate(o.getSelectedDepartureDate());
                        o.getCurrentCustomerOrder("2").setReturnDate(o.getSelectedReturnDate());

                        new Database().set(o);
                        System.out.println("Valt datum: " + new Database().get().getSelectedDepartureDate().getTime());
                        Hyttalternativ_GUI.hyttAlternativ();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(BookingPage1.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            }
        });

        //en bakåt knapp för att gå till förgående steg
        JButton back = new JButton("Bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        frame.add(back);
        back.setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    MainProgramPage.pageGUI();

                }

            }
        });

        listChoice.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                System.out.println(e.getItem());
                o.setSelectedDepartureDate(e.getItem().toString());
                datumet.setText(o.getArrivalDate(o.getSelectedDepartureDate()).getTime().toString());

            }
        });

        chkBoxHealthInsurance.addItemListener(new ItemListener() {
            String prev = txtSumField.getText();

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (chkBoxHealthInsurance.isSelected()) {
                    int P2healthInsurance = chkBoxHealthInsuranceP2.isSelected() ? 2 : 1;
                    txtSumField.setText(String.valueOf(o.getArticle().get("Hälsoförsäkring").getPrice() * P2healthInsurance) + " SEK");
                    //o.getCurrentCustomerOrder("1").setHealthInsurance(true);
                } else {
                    int P2healthInsurance = chkBoxHealthInsuranceP2.isSelected() ? 1 : 0;
                    txtSumField.setText(String.valueOf(o.getArticle().get("Hälsoförsäkring").getPrice() * P2healthInsurance) + " SEK");
                };

            }
        });

        chkBoxHealthInsuranceP2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (chkBoxHealthInsuranceP2.isSelected()) {
                    int P1healthInsurance = chkBoxHealthInsurance.isSelected() ? 2 : 1;

                    txtSumField.setText(String.valueOf(o.getArticle().get("Hälsoförsäkring").getPrice() * P1healthInsurance) + " SEK");
                    //o.getCurrentCustomerOrder("1").setHealthInsurance(true);
                } else {
                    int P1healthInsurance = chkBoxHealthInsurance.isSelected() ? 1 : 0;
                    txtSumField.setText(String.valueOf(o.getArticle().get("Hälsoförsäkring").getPrice() * P1healthInsurance) + " SEK");

                };

            }
        });

        ///LAGRADE KUNDUPPGIFTER
        if (!(o.getCurrentCustomer("1") == null)) {

            txtBirthdate.setText(o.getCurrentCustomer("1").getIdentityNumber());
            txtfirstName.setText(o.getCurrentCustomer("1").getFirstName());
            txtLastName.setText(o.getCurrentCustomer("1").getLastName());
            txtAdress.setText(o.getCurrentCustomer("1").getAdress());
            txtEmail.setText(o.getCurrentCustomer("1").getEmail());
            txtTelefon.setText(o.getCurrentCustomer("1").getPhoneNumber());
            txtKronisk.setText(o.getCurrentCustomer("1").getHealthDescription());

            if (o.getCurrentCustomerOrder("1").isHealthInsurance()) {
                chkBoxHealthInsurance.doClick();
                txtSumField.setText(String.valueOf(o.getArticle().get("Hälsoförsäkring").getPrice()) + " SEK");
            } 

        }     

        //storlek och properties för ramen
        frame.add(mainPanel);
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
