/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import marstravels.Database;
import marstravels.OrderProcessor;

/**
 *
 * @author emile
 */
public class Matpaket_GUI {

    public static void matPaket() throws IOException, ClassNotFoundException, FileNotFoundException {

        OrderProcessor o = new Database().get();

        // skapar ramen frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblHeaderPage4 = new JLabel("Val av matpaket");
        lblHeaderPage4.setFont(new Font("Serif", Font.PLAIN, 32));
        lblHeaderPage4.setBounds(350, 0, 800, 150);
        //lblHeaderPage2.setLocation(400, 0);
        frame.add(lblHeaderPage4);

        JTextArea infoRuta = new JTextArea("MarsDonald har 2 olika frukostar, 3 olika luncher och 4 olika middagar,"
                + "\n"
                + "samt 3 olika kakor/desserter att tillgå mellan måltiderna. "
                + "\n"
                + "Marsian Buffé är restaurangen som har buffé dygnet runt"
                + "\n"
                + " med olika rätter som kan passa oavsett tid på dagen."
                + "\n"
                + "Alltifrån våfflor och omeletter till grillat och pizza."
                + "\n"
                + "Tellus Home serverar en enklare frukostbuffé, 5 olika luncher "
                + "\n"
                + "(kött/fisk/veg/soppa/pasta) och 6 olika middagar"
                + " (2 alternativ av fisk/kött/veg). "
                + "\n"
                + "Deras desserbuffé som är öppen mellan måltiderna består av "
                + "3-5 olika kakor/desserter plus dryck. "
                + "\n"
                + "Spaceview restaurangen har specialiserat sig på ny och kreativ mat "
                + "\n"
                + "där kockarna tar favoriträtter från jorden "
                + "\n"
                + "och med inspiration från rymden gör om dem och ger dig en ny smakupplevelse. ");
        infoRuta.setBounds(350, 100, 570, 250);
        infoRuta.setVisible(false);
        infoRuta.setEditable(false);
        frame.add(infoRuta);
        
        JTextArea infoRuta2 = new JTextArea("Budget 1: 27 000 kr/pers  - Samtliga måltider under resan intas på MarsDonalds."
                + "\n"
                + "Budget 2: 40 000 kr/pers - All frukost och lunch intas på "
                + "\n"
                + "MarsDonalds medan mellanmål och middagarna intas på Marsian Buffé."
                + "\n"
                + "Budget 3:  54 000 kr/per - Samtliga måltider under resan intas på Marsian Buffé."
                + "\n"
                + "Mellan 1: 76 000 kr/pers  All frukost intas på MarsDonalds, "
                + "\n"
                + "mellanmål och lunch på Marsian Buffé medan middagarna intas på Tellus Home."
                + "\n"
                + "Mellan 2: 90 000 kr/pers - Alla måltider förutom middag intas på Marsian Buffé. "
                + "\n"
                + "Middagarna intas på Tellus Home."
                + "\n"
                + "Mellan 3: 100 000 kr/pers - Samtliga måltider intas på Tellus Home."
                + "\n"
                + "Lyx 1: 120 000 kr/pers - All måltider förutom middag intas på Tellus Home "
                + "\n"
                + "medan middagarna intas på SpaceView."
                + "\n"
                + "Lyx 2: 150 000 kr/pers - Frukost och mellanmål intas på Tellus Home "
                + "\n"
                + "medan lunch och middag intas på SpaceView."
                + "\n"
                + "Lyx 3: 200 000 kr/pers -Samtliga måltider intas på SpaceView. "
                + "\n"
                + "För de som valt hyttalternativet Svit serveras frukosten i hytten från SpaceView."
                
        );
        infoRuta2.setBounds(350, 100, 570, 250);
        infoRuta2.setVisible(false);
        infoRuta2.setEditable(false);
        frame.add(infoRuta2);

        //öppnar o stänger inforutan
        JToggleButton toggleButton = new JToggleButton("Visa info");
        toggleButton.setBounds(150, 150, 150, 50);
        frame.add(toggleButton);
        ItemListener itemListener = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRuta.setVisible(true); // show your message here
                    infoRuta2.setVisible(false);
                } else {
                    infoRuta.setVisible(false); // remove your message
                }
            }
        };
        toggleButton.addItemListener(itemListener);
        
           JToggleButton toggleButton2 = new JToggleButton("Visa info om mat");
        toggleButton2.setBounds(150, 200, 150, 50);
        frame.add(toggleButton2);
        ItemListener itemListener2 = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    infoRuta2.setVisible(true); // show your message here
                    infoRuta.setVisible(false);
                } else {
                    infoRuta2.setVisible(false); // remove your message
                }
            }
        };
        toggleButton2.addItemListener(itemListener2);

        //gör en rulllista som heter budget (set bounds bestämmer storlek och location)
        final Choice budget = new Choice();
        budget.setBounds(10, 50, 75, 75);
        budget.setLocation(50, 90);
//här fyller man i vad lista ska bestå av

        frame.add(budget);

//samma som ovan fast rullistan heter lyx
        final Choice lyx = new Choice();
        lyx.setBounds(10, 50, 75, 75);
        lyx.setLocation(50, 90);

        lyx.setVisible(false);
        frame.add(lyx);

        o.getArticle().values().forEach((article) -> {
            if (article.getCategory().equals("Matpaket")) {
                budget.add(article.getName());
                lyx.add(article.getName());
            }
        });

        //ruta som visar person 1
        JLabel lblPerson1 = new JLabel("Resenär 1");
        lblPerson1.setSize(70, 10);
        lblPerson1.setLocation(50, 60);
        frame.add(lblPerson1);

        //ruta som visar person 2
        JLabel lblPerson2 = new JLabel("Resenär 2");
        lblPerson2.setSize(70, 10);
        lblPerson2.setLocation(50, 60);
        lblPerson2.setVisible(false);
        frame.add(lblPerson2);

        //knappen som tar dig vidare till nästa sida
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        evenemang_GUI.evenemang();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Matpaket_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        JLabel lblInfo = new JLabel("Information om matpaketen");
        lblInfo.setBounds(150, 80, 200, 100);
        frame.add(lblInfo);

        //en bakåtknapp 8gå till förgående steg)
        JButton back = new JButton("bakåt");
        back.setSize(70, 50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        Hyttalternativ_GUI.hyttAlternativ();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Matpaket_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //ruatn som visar totalsumman
        JTextField txtSumField = new JTextField();
        txtSumField.setBounds(100, 100, 100, 50);
        txtSumField.setLocation(450, 400);
        txtSumField.setEditable(false);
        frame.add(txtSumField);

        JButton buttonPerson2 = new JButton("P2");
        buttonPerson2.setBounds(100, 15, 50, 30);
        buttonPerson2.setVisible(true);
        frame.add(buttonPerson2);
        buttonPerson2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson2.setVisible(true);
                    lyx.setVisible(true);
                    lblPerson1.setVisible(false);
                    budget.setVisible(false);

                } else {
                    lblPerson2.setVisible(false);
                    lyx.setVisible(false);

                }
            }
        });

        JButton buttonPerson1 = new JButton("P1");
        buttonPerson1.setBounds(50, 15, 50, 30);
        frame.add(buttonPerson1);
        buttonPerson1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (buttonPerson2.isEnabled()) {
                    lblPerson1.setVisible(true);
                    budget.setVisible(true);
                    lblPerson2.setVisible(false);
                    lyx.setVisible(false);

                } else {
                    lblPerson1.setVisible(false);
                    budget.setVisible(false);

                }
            }
        });

        budget.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                o.getCurrentCustomer();
                o.getCurrentCustomerOrder("1").setFoodPackage(e.getItem().toString());

                txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
                try {
                    new Database().set(o);
                } catch (IOException ex) {
                    Logger.getLogger(Hyttalternativ_GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        //rutan som visar att totalsumman ska visas bredvid
        JLabel lblTotala = new JLabel("Totalsumma:");
        lblTotala.setBounds(300, 500, 150, 150);
        lblTotala.setLocation(350, 350);
        frame.add(lblTotala);

        //denhär gör inget... men om man tar bort den hanar labeln ovanför fel.
        JLabel lblTotalSum = new JLabel();
        lblTotalSum.setBounds(300, 500, 150, 150);
        lblTotalSum.setLocation(350, 350);
        frame.add(lblTotalSum);

        txtSumField.setText(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
        System.out.println("ferry_package: " + o.getCurrentCustomerOrder("1").getFerryRoom());
        //properties för ramen
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

   

}
