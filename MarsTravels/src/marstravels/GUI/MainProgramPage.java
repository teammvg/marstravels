/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Sidan som kommer efter inloggningen och som innehåller valen Skapa bokning,
 * söka bokning och se statistik man kan också logga ut som användare.
 *
 * @author nilsi
 */
public class MainProgramPage extends JFrame {

    public static void main(String args[]) throws IOException, ClassNotFoundException {



       LogIn_GUI.password();
     //pageGUI();

    }

    public static void pageGUI() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

          JLabel lblHeaderPage1 = new JLabel("Mars Travels");
        lblHeaderPage1.setFont(new Font("Serif", Font.PLAIN, 50));
        lblHeaderPage1.setBounds(370, 70, 2000, 500);
        //lblHeaderPage1.setLocation(400, 0);
        frame.add(lblHeaderPage1);
        
        
        JPanel mainPanel;
        mainPanel = new JPanel(new GridBagLayout());
        JTextField txtTraveler;

        //knapp för bokning
        JButton booking = new JButton("Bokning");
        booking.setSize(70, 50);
        booking.setBounds(100, 50, 180, 130);
        booking.setVisible(true);
        frame.add(booking);

        booking.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (booking.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        BookingPage1.pageGUI();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(MainProgramPage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        //en bakåt knapp för att gå till förgående steg
        JButton back = new JButton("Logga ut");
        back.setSize(70, 50);
        back.setBounds(435, 500, 100, 50);
        frame.add(back);
        back.setVisible(true);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    LogIn_GUI.password();

                }

            }
        });
        //knapp för att söka boning
        JButton bookingSearch = new JButton("Söka bokning");
        bookingSearch.setSize(70, 50);
        bookingSearch.setBounds(400, 50, 180, 130);
        bookingSearch.setVisible(true);
        frame.add(bookingSearch);
     bookingSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (bookingSearch.isEnabled()) {
                    frame.setVisible(false);
                   SökaBokning.searchBokning();

                }

            }
        });
        
        
        
        

        //knappen för att se statistik
        JButton statistics = new JButton("Se statistik");
        statistics.setSize(70, 50);
        statistics.setBounds(700, 50, 180, 130);
        statistics.setVisible(true);
        frame.add(statistics);

        //mått på ramen
        // frame.add(c);
        frame.add(mainPanel);
        frame.setSize(1000, 650);
        frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
