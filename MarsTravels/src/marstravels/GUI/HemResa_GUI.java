/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels.GUI;

import java.awt.Choice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import marstravels.Database;
import marstravels.OrderProcessor;


/**
 *
 * @author emile
 */
public class HemResa_GUI {
    
public static void hemResa() throws IOException, ClassNotFoundException, FileNotFoundException{
    
    OrderProcessor o = new Database().get();
 // skapar ramen frame
JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JCheckBox kPerson1, kPerson2, cPerson1, cPerson2;
        
        kPerson1 = new JCheckBox("Kopiera person 1");
        kPerson1.setBounds(10, 10, 200, 50);
        kPerson1.setLocation(50, 85);
        frame.add(kPerson1);
        
        kPerson2 = new JCheckBox("Kopiera person 2");
        kPerson2.setBounds(10, 10, 200, 50);
        kPerson2.setLocation(50, 120);
        frame.add(kPerson2);
        
         cPerson1 = new JCheckBox("Ändra val för person 1");
        cPerson1.setBounds(10, 10, 200, 50);
        cPerson1.setLocation(50, 155);
        frame.add(cPerson1);
        
         cPerson2 = new JCheckBox("Ändra val för person 2");
        cPerson2.setBounds(10, 10, 200, 50);
        cPerson2.setLocation(50, 190);
        frame.add(cPerson2);
        

          
        /*
         //ruta som visar person 2
         JLabel lblPerson21 = new JLabel("Person 2");
        lblPerson21.setBounds(100, 100, 100, 50);
        lblPerson21.setLocation(50, 170);
        frame.add(lblPerson21);
        */
        
               //knappen som tar dig vidare till nästa sida
        JButton fram = new JButton("Fortsätt");
        fram.setSize(70, 50);
        fram.setBounds(750, 500, 100, 50);
        fram.setVisible(true);
        frame.add(fram);

        fram.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fram.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        ConfirmationPage.pageGUI();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(HemResa_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

             //en bakåtknapp 8gå till förgående steg)
        JButton back = new JButton("bakåt");
        back.setSize(70,50);
        back.setBounds(100, 500, 100, 50);
        back.setVisible(true);
          frame.add(back);
          
                    back.addActionListener(new ActionListener() {  
            public void actionPerformed(ActionEvent e) {    
                if (back.isEnabled()) {
                    frame.setVisible(false);
                    try {
                        HotellOnMars_GUI.hotellOnMars();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(HemResa_GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
        }
       
        }  
        });
        
          //ruatn som visar totalsumman
        JTextField txtField = new JTextField(String.valueOf(o.getTotalCost(o.getCurrentCustomerOrder("1"))) + " SEK");
        txtField.setBounds(100, 100, 100, 50);
        txtField.setLocation(450, 400);
         txtField.setEditable(false);
        frame.add(txtField);
        
         JLabel lbltotale = new JLabel("Totalsumma:");
        lbltotale.setBounds(100, 100, 100, 50);
        lbltotale.setLocation(350, 400);
        frame.add(lbltotale);
           
           //ruta som visar inget, ta inte bort
        JLabel lblTotal = new JLabel();
        lblTotal.setBounds(100, 100, 100, 50);
        lblTotal.setLocation(350, 400);
        frame.add(lblTotal);
        
 //properties för ramen
          frame.setSize(1000, 650);
    frame.setLocation(50, 5);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
}
}
