/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marstravels;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Elias
 */
public class Database {
    
    transient FileInputStream fileIn;
    transient FileOutputStream fileOut;
    transient ObjectOutputStream objectOut;
    transient ObjectInputStream objectIn;

    public void set(OrderProcessor o) throws FileNotFoundException, IOException {
        this.fileOut = new FileOutputStream("OrderProcessor.ser");
        this.objectOut = new ObjectOutputStream(fileOut);
        this.objectOut.writeObject(o);
    }

    public OrderProcessor get() throws FileNotFoundException, IOException, ClassNotFoundException {
        this.fileIn = new FileInputStream("OrderProcessor.ser");
        this.objectIn = new ObjectInputStream(fileIn);
        OrderProcessor o = (OrderProcessor) objectIn.readObject();
        return o;
    }
    
}
