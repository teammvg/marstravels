package marstravels;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 *
 * @author Elias
 */
public class OrderProcessor implements Serializable {

    private final LinkedHashMap<String, Calendar> departureDate = new LinkedHashMap();
    private final LinkedHashMap<String, Calendar> returnDate = new LinkedHashMap();
    private final LinkedHashMap<String, CustomerOrder> departureOrder = new LinkedHashMap();
    private final LinkedHashMap<String, CustomerOrder> returnOrder = new LinkedHashMap();
    private final HashMap<String, Customer> customer = new HashMap();
    private final HashMap<String, Article> article = new HashMap();

    private final LinkedHashMap<String, Customer> currentCustomer = new LinkedHashMap();
    private final LinkedHashMap<String, CustomerOrder> currentCustomerOrder = new LinkedHashMap();
    private Calendar selectedDepartureDate;
    private Calendar selectedReturnDate;

    public OrderProcessor() {

    }

    public LinkedHashMap<String, Calendar> getDepartureDate() {
        return departureDate;
    }

    public LinkedHashMap<String, CustomerOrder> getDepartureOrder() {
        return departureOrder;
    }

    public CustomerOrder getDepartureOrder(String customerId) {
        return departureOrder.get(customerId);
    }

    public LinkedHashMap<String, CustomerOrder> getReturnOrder() {
        return returnOrder;
    }

    public CustomerOrder getReturnOrder(String customerId) {
        return returnOrder.get(customerId);
    }

    public LinkedHashMap<String, CustomerOrder> getOrderList(String type) {
        if (type.equals("departure")) {
            return this.getDepartureOrder();
        } else if (type.equals("return")) {
            return this.getReturnOrder();
        }
        return null;
    }

    public HashMap<String, Customer> getCustomer() {
        return customer;
    }

    public Customer getCustomer(String customerId) {
        return customer.get(customerId);
    }

    public HashMap<String, Article> getArticle() {
        return article;
    }

    public void setCustomer(String customerId, String firstName, String lastName, String adress, String identityNumber, String email, String phoneNumber, String healthDescription) {
        Customer c = new Customer(customerId, firstName, lastName, adress, identityNumber, email, phoneNumber, healthDescription);
        this.getCustomer().put(c.getCustomerId(), c);
    }

    public void setDepartureOrder(String customerId) {
        CustomerOrder c = new CustomerOrder(customerId);
        this.getDepartureOrder().put(customerId, c);
    }

    public void setReturnOrder(String customerId) {
        CustomerOrder c = new CustomerOrder(customerId);
        this.getDepartureOrder().put(customerId, c);
    }

    public void setArticle(String name, String category, int price, int amount) {
        Article a = new Article(name, category, price, amount);
        this.getArticle().put(name, a);
    }

    public void setDeparture(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, day, 12, 00, 00);
        this.getDepartureDate().put(c.getTime().toString(), c);
        Calendar r = Calendar.getInstance();
        r.set(c.get(1), c.get(2) + 6, c.get(5), 16, 0, 0);
        this.setReturn(r);
    }

    public void setReturn(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, day, 12, 00, 00);
        this.getReturnDate().put(c.getTime().toString(), c);
    }

    public void setReturn(Calendar date) {
        this.getReturnDate().put(date.getTime().toString(), date);
    }

    public Calendar getArrivalDate(String orderId) {
        return this.getDateFromDepture(orderId, 6);
    }

    public Calendar getArrivalDate(Calendar date) {
        return this.getDateFromDepture(date, 6);
    }

    public Calendar getCalendarDate(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, day, 12, 00, 00);
        return c;
    }

    public Calendar getDateFromDepture(String orderId, int monthAmount) {
        Calendar c = Calendar.getInstance();
        CustomerOrder order = this.getDepartureOrder(orderId);
        c.set(order.getDepartureDate().get(1), order.getDepartureDate().get(2) + monthAmount, order.getDepartureDate().get(5), 14, 0, 0);
        return c;
    }

    public Calendar getDateFromDepture(Calendar date, int monthAmount) {
        Calendar c = Calendar.getInstance();
        c.set(date.get(1), date.get(2) + monthAmount, date.get(5), 14, 0, 0);
        return c;
    }

    public LinkedHashMap<String, Integer> getReservedFerryRoomAmountList(String date, String type) {

        LinkedHashMap<String, CustomerOrder> order = new LinkedHashMap();
        Calendar ferryDate = this.getDepartureDate().get(date);

        if (type.equals("departure")) {
            order = this.getDepartureOrder();
        } else if (type.equals("return")) {
            order = this.getReturnOrder();
        }
        LinkedHashMap<String, Integer> reservedFerryRoomArticle = new LinkedHashMap();

        order.values().forEach((customerOrder) -> {
            if (customerOrder.getDepartureDate().equals(ferryDate)) {

                String ferryRoom = customerOrder.getFerryRoom();

                reservedFerryRoomArticle.putIfAbsent(ferryRoom, reservedFerryRoomArticle.getOrDefault(ferryRoom, 0) + 1);

            }
        });

        return reservedFerryRoomArticle;

    }

    public LinkedHashMap<String, Integer> getArticleAmountList(String category) {
        LinkedHashMap<String, Integer> articleAmountList = new LinkedHashMap();
        this.getArticle().values().forEach((art) -> {
            if (art.getCategory().equals(category)) {
                articleAmountList.put(art.getName(), art.getAmount());
            }
        });
        return articleAmountList;
    }

    public LinkedHashMap<String, Integer> getReservedHotelRoomList(Calendar fromDate, Calendar toDate) {
        LinkedHashMap<String, Integer> reservedHotelRoomList = new LinkedHashMap();

        this.departureOrder.values().forEach((order) -> {
            if (isWithinPeriod(fromDate, order.getArrivalDate(), order.getReturnDate())
                    || isWithinPeriod(toDate, order.getArrivalDate(), order.getReturnDate())
                    || isWithinPeriod(order.getArrivalDate(), fromDate, toDate)
                    || isWithinPeriod(order.getReturnDate(), fromDate, toDate)) {
                String hotelRoomReservation = order.getHotelRoom();
                reservedHotelRoomList.putIfAbsent(hotelRoomReservation, reservedHotelRoomList.getOrDefault(hotelRoomReservation, 0) + 1);
            }
        });
        return reservedHotelRoomList;
    }

    public boolean isWithinPeriod(Calendar searchDate, Calendar dateBegin, Calendar dateEnd) {
        return searchDate.after(dateBegin) && searchDate.before(dateEnd);
    }

    public LinkedHashMap<String, Integer> getAvailableFerryRoomList(String date, String type) {
        LinkedHashMap<String, Integer> availableArticleList = this.getArticleAmountList("Hytt");

        this.getReservedFerryRoomAmountList(date, type).entrySet().forEach((entry) -> {
            availableArticleList.replace(entry.getKey(), availableArticleList.get(entry.getKey()) - entry.getValue());
        });

        return availableArticleList;
    }

    public LinkedHashMap<String, Integer> getAvailableHotelRoomList(Calendar fromDate, Calendar toDate) {
        LinkedHashMap<String, Integer> availableArticleList = this.getArticleAmountList("Hotell");

        this.getReservedHotelRoomList(fromDate, toDate).entrySet().forEach((entry) -> {
            availableArticleList.replace(entry.getKey(), availableArticleList.get(entry.getKey()) - entry.getValue());
        });

        return availableArticleList;
    }

    public int getTotalCost(String orderId, String type) {
        CustomerOrder order = this.getOrderList(type).get(orderId);
        int totalCost = 0;

        totalCost += this.article.get(order.getFerryRoom()).getPrice();
        totalCost += this.article.get(order.getFoodPackage()).getPrice();
        totalCost += this.article.get(order.getMovieTicket()) == null ? 0 : this.article.get(order.getMovieTicket()).getPrice();
        totalCost += this.article.get(order.getTheatreTicket()) == null ? 0 : this.article.get(order.getTheatreTicket()).getPrice();
        totalCost += this.article.get(order.getConsertTicket()) == null ? 0 : this.article.get(order.getConsertTicket()).getPrice();
        totalCost += order.getDebitCard();

        if (type.equals("departure")) {
            totalCost += order.isHealthInsurance() ? this.article.get("Hälsoförsäkring").getPrice() : 0;
            totalCost += this.article.get(order.getHotelRoom()).getPrice();
        }

        return totalCost;
    }

    public int getTotalCost(CustomerOrder o) {
        CustomerOrder order = o;
        int totalCost = 0;

        totalCost += this.article.get(order.getFerryRoom()) == null ? 0 : this.article.get(order.getFerryRoom()).getPrice();
        totalCost += this.article.get(order.getFoodPackage()) == null ? 0 : this.article.get(order.getFoodPackage()).getPrice();
        totalCost += this.article.get(order.getMovieTicket()) == null ? 0 : this.article.get(order.getMovieTicket()).getPrice();
        totalCost += this.article.get(order.getTheatreTicket()) == null ? 0 : this.article.get(order.getTheatreTicket()).getPrice();
        totalCost += this.article.get(order.getConsertTicket()) == null ? 0 : this.article.get(order.getConsertTicket()).getPrice();
        totalCost += order.getDebitCard();
        totalCost += order.isHealthInsurance() ? this.article.get("Hälsoförsäkring").getPrice() : 0;
        totalCost += this.article.get(order.getHotelRoom()) == null ? 0 : this.article.get(order.getHotelRoom()).getPrice() * ((ChronoUnit.DAYS.between(order.getDepartureDate().toInstant(), o.getReturnDate().toInstant()) / 30) - 6);

        return totalCost;
    }

    public LinkedHashMap<String, Customer> getCurrentCustomer() {
        return currentCustomer;
    }

    public Customer getCurrentCustomer(String id) {
        return currentCustomer.get(id);
    }

    public void setCurrentCustomer(String id, Customer currentCustomer) {
        this.currentCustomer.put(id, currentCustomer);
    }

    public LinkedHashMap<String, CustomerOrder> getCurrentCustomerOrder() {
        return currentCustomerOrder;
    }

    public CustomerOrder getCurrentCustomerOrder(String id) {
        return currentCustomerOrder.get(id);
    }

    public void setCurrentCustomerOrder(String id, CustomerOrder currentCustomerOrder) {
        this.currentCustomerOrder.put(id, currentCustomerOrder);
    }

    public Calendar getSelectedDepartureDate() {
        return selectedDepartureDate;
    }

    public void setSelectedDepartureDate(Calendar selectedDepartureDate) {
        this.selectedDepartureDate = selectedDepartureDate;
    }

    public void setSelectedDepartureDate(String selectedDepartureDate) {
        this.selectedDepartureDate = this.getDepartureDate().get(selectedDepartureDate);
    }

    public Calendar getSelectedReturnDate() {
        return selectedReturnDate;
    }

    public void setSelectedReturnDate(Calendar selectedReturnDate) {
        this.selectedReturnDate = selectedReturnDate;
    }

    public void setSelectedReturnDate(String selectedReturnDate) {
        this.selectedReturnDate = this.getReturnDate().get(selectedReturnDate);
    }

    public LinkedHashMap<String, Calendar> getReturnDate() {
        return returnDate;
    }

}
