package marstravels;

import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedHashMap;


/**
 *
 * @author Elias
 */
public class MarsTravels {

    public static void main(String[] args) throws IOException {

        OrderProcessor o = new OrderProcessor();

        int currentMonth = 3;
        for (int i = 0; i < 12; i++) {
            o.setDeparture(2018, 3 + i, 12);
        }
        //Artiklar ..........
        o.setArticle("Hälsoförsäkring", "Hälsoförsäkring", 50000, 0);


        o.setArticle("Sömnkapsel", "Hytt", 2500000, 1);
        o.setArticle("Svit", "Hytt", 1200000, 2);
        o.setArticle("Spaceside", "Hytt", 700000, 4);
        o.setArticle("Inside", "Hytt", 300000, 8);
        o.setArticle("Economy", "Hytt", 180000, 16);

        o.setArticle("Budget1", "Matpaket", 27000, 0);
        o.setArticle("Budget2", "Matpaket", 40000, 0);
        o.setArticle("Budget3", "Matpaket", 54000, 0);
        o.setArticle("Mellan1", "Matpaket", 76000, 0);
        o.setArticle("Mellan2", "Matpaket", 90000, 0);
        o.setArticle("Mellan3", "Matpaket", 100000, 0);
        o.setArticle("Lyx1", "Matpaket", 120000, 0);
        o.setArticle("Lyx2", "Matpaket", 150000, 0);
        o.setArticle("Lyx3", "Matpaket", 200000, 0);

        o.setArticle("Filmbiljett 1", "Evenemangsbiljett", 210, 0);
        o.setArticle("Filmbiljett 2", "Evenemangsbiljett", 210*2, 0);
        o.setArticle("Filmbiljett 3", "Evenemangsbiljett", 210*3, 0);
        o.setArticle("Filmbiljett 4", "Evenemangsbiljett", 210*4, 0);
        o.setArticle("Filmbiljett 5", "Evenemangsbiljett", 210*5, 0);
        o.setArticle("Filmbiljett 6", "Evenemangsbiljett", 210*6, 0);


        o.setArticle("Teaterbiljett 1", "Evenemangsbiljett", 380, 0);
        
       o.setArticle("Teaterbiljett 2", "Evenemangsbiljett", 380*2, 0);
   o.setArticle("Teaterbiljett 3", "Evenemangsbiljett", 380*3, 0);



        o.setArticle("Konsertbiljett 1", "Evenemangsbiljett", 260*1, 0);

        o.setArticle("Konsertbiljett 2", "Evenemangsbiljett", 260*2, 0);



        o.setArticle("Konsertbiljett 3", "Evenemangsbiljett", 260*3, 0);




        o.setArticle("Bädd Polar Lansdorp", "Hotell", 3500, 8);
        o.setArticle("Bädd Polar Wielders", "Hotell", 5000, 8);

        o.setArticle("Enkelrum Hotel Phobos", "Hotell", 7500, 4);
        o.setArticle("Dubbelrum Hotel Phobos", "Hotell", 12000, 4);

        o.setArticle("Enkelrum Hotel Deimos", "Hotell", 7500, 4);
        o.setArticle("Dubbelrum Hotel Deimos", "Hotell", 12000, 2);

        o.setArticle("Enkel Lyx Royal City", "Hotell", 20000, 4);
        o.setArticle("Dubbel Lyx Royal City", "Hotell", 35000, 2);
        o.setArticle("Svit Royal City", "Hotell", 50000, 1);

        //Test kund..........
        o.setCustomer("9211240214", "Elias",  "Test", "Testvägen 12", "9211240214", "Test@Testa.se", "0722679907", "Okänd");
        o.setDepartureOrder("9211240214");

        //Test lagrad-order
        o.setCurrentCustomer("1", new Customer("1", "Test",  "Testman", "Testvägen 12", "9211240438", "Test@Testa.se", "0722678809", "Nej"));
        o.setCurrentCustomerOrder("1", new CustomerOrder("1"));


        System.out.println(o.getCustomer().get("9211240214").getFullName());

        String depDate = "Sat May 12 12:00:00 CEST 2018";

        //Sätter avresedatum
        o.getDepartureOrder("9211240214").setDepartureDate(o.getDepartureDate().get(depDate));

        //Lägg till hälsoförsäkring
        o.getDepartureOrder("9211240214").setHealthInsurance(true);

        //Lägg till hytt
        o.getDepartureOrder("9211240214").setFerryRoom("Inside");

        //Lägg till matpket
        o.getDepartureOrder("9211240214").setFoodPackage("Lyx1");

        //Lägg till biljett
        o.getDepartureOrder("9211240214").setMovieTicket("");
        o.getDepartureOrder("9211240214").setTheatreTicket("");
        o.getDepartureOrder("9211240214").setConsertTicket("");

        //Lägg till marshotell
        o.getDepartureOrder("9211240214").setHotelRoom("Enkel Lyx Royal City");

        //Sätt hemresedatum
        o.getDepartureOrder("9211240214").setReturnDate(o.getDateFromDepture("9211240214", 8));

        //Lista lediga rum inom ett avresedatum
        LinkedHashMap<String, Integer> availableFerryRoomList = o.getAvailableFerryRoomList(depDate, "departure");
        availableFerryRoomList.entrySet().forEach((entry) -> {
            System.out.println(entry.getKey() + " Lediga: " + entry.getValue());
        });

        //Lista lediga hotelrum inom ett avresedatum
        Calendar fromDate = o.getCalendarDate(2018, 3, 12);
        Calendar toDate = o.getCalendarDate(2018, 10, 12);
        
        System.out.println(o.getAvailableHotelRoomList(fromDate, toDate).entrySet());
        
        
        Database d = new Database();
        
        d.set(o);

    }

}
