package marstravels;

import java.io.Serializable;

/**
 *
 * @author Elias
 */
public class Article implements Serializable {

    private String name;
    private String category;
    private String description;
    private int price;
    private int amount;

    public Article() {

    }

    public Article(String name, String category, int price, int amount) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
